import * as Blockly from "blockly";
import { Colors } from "../../colors";
import { Types } from "../types";

export const step_definition_placeholder = {
  type: "step_definition_placeholder",
  message0: "step definition",
  output: [
    Types.ProcedureMainInitiateAndConfirmStepDefinition,
    Types.ProcedureWatchdogInitiateAndConfirmStepDefinition,
  ],
  colour: Colors.BlockPlaceholderRequired,
};
export const continuation_test_placeholder =
{
  type: "continuation_test_placeholder",
  message0: "continuation test",
  output: Types.ProcedureMainInitiateAndConfirmStepContinuationTest,
  //colour: Colors.CategoryStep,
};
export const refer_by_placeholder =
{
  type: "refer_by_placeholder",
  message0: "refer by",
  output: Types.ProcedureMainInitiateActivityReferBy,
  //colour: Colors.BlockPlaceholder,
};
export const described_by_placeholder = {
  type: "described_by_placeholder",
  message0: "described by",
  output: [
    Types.ProcedureDeclarationDescribedBy,
    Types.StepDeclarationDescribedBy
  ],
  //colour: Colors.BlockPlaceholder,
};
export const expression_placeholder = {
  type: "expression_placeholder",
  message0: "expression",
  output: [
    Types.ProcedurePreconditionsExpression,
    Types.ProcedureConfirmationExpression,
    Types.StepPreconditionsExpression,
    Types.StepConfirmationExpression,
    Types.StepMainExpression,
  ],
  colour: Colors.BlockPlaceholderRequired,
};
export const timeout_placeholder = {
  type: "timeout_placeholder",
  message0: "timeout",
  output: [
    Types.ProcedurePreconditionsTimeout,
    Types.ProcedureConfirmationTimeout,
    Types.StepConfirmationTimeout,
    Types.StepPreconditionsTimeout,
    Types.StepMainRepeatTimeout,
    Types.StepMainWaitTimeout,
  ],
  //colour: Colors.CategoryStep,
};
export const raise_event_placeholder = {
  type: "raise_event_placeholder",
  message0: "raise event",
  output: [
    Types.ProcedurePreconditionsRaiseEvent,
    Types.ProcedureConfirmationRaiseEvent,
    Types.StepConfirmationRaiseEvent,
    Types.StepPreconditionsRaiseEvent,
    Types.StepMainRaiseEvent,
  ],
  //colour: Colors.CategoryStep,
};
