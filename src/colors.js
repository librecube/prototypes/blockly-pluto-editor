export const Colors = {
  CategoryProcedure: 200,
  CategoryStep: 250,
  CategoryWatchdog: 300,

  BlockDeclaration: 0,
  BlockPreconditions: 40,
  BlockMain: 80,
  BlockWatchdog: 120,
  BlockConfirmation: 160,

  BlockPlaceholderRequired: 0,
  BlockPlaceholder: 100,

  // TODO: Use those colors for blocks instead:
  BlockWait: 0,
  BlockExpression: 0,
  BlockValue: 0,
  BlockLog: 0,
  BlockInformUser: 0,
  BlockInitiate: 0,
  BlockEvent: 0,
  BlockStep: 0,
  BlockProcedure: 0,
  BlockVariable: 0,
  BlockFlowControl: 0,
  BlockAssignment: 0,
};
