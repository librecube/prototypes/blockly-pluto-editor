# Adding custom block to app
---

> In this tutorial we will understand how to add custom block.

- **Step 1: Define the Block**

First, you need to define the new block. You can do this by adding Json definition for the block like this:

```javascript

const initiate_and_confirm_activity = {
  "type": "initiate_and_confirm_activity",
  "message0": "initiate and confirm %1 %2",
  "args0": [
    {
      "type": "field_input",
      "name": "value",
      "text": "activity name"
    },
    {
      "type": "input_value",
      "name": "extra"
    }
  ],
  "previousStatement": null,
  "nextStatement": null,
  "colour": 30,
  "tooltip": "Initiate And Confirm Activity Statement ",
  "helpUrl": ""
}

```
in above example
 - `type` defines name of the block
 - `message0` contains string about blocks display message, blocks inputs and fields. Here "initiate and confirm" string is shown on the block and 2 inputs are provided %1 is for activity name and %2 is optional blocks which can be attached to this block.
 - `args0` contains array of inputs and fields.
 -  `previousStatement` defines the type of block that can connect to the top of the current block. If a block has a previousStatement property, it means that another block can connect to its top.
 -  `nextStatement` defines the type of block that can connect to the bottom of the current block. If a block has a previousStatement property, it means that another block can connect to its top.
 -  `colour` defines color for blocks.
 -  `tooltip` provides more information about block.

also you can define blocks using javascript object [see here](https://developers.google.com/blockly/guides/create-custom-blocks/define-blocks)
you can use [Blockly developer tools](https://blockly-demo.appspot.com/static/demos/blockfactory/index.html) to create custom blocks easily.
[How to use Blockly Developer Tools](https://developers.google.com/blockly/guides/create-custom-blocks/blockly-developer-tools)
[More info about creating custom blocks](https://developers.google.com/blockly/guides/create-custom-blocks/overview)

- **Step 2: add block to definition**
    - firstly create block using above step then go `./src/blocks` and place it in appropriate file
    - then in the `./src/blocks/pluto.js` register that block in
  ```javascript
  export const blocks = Blockly.common.createBlockDefinitionsFromJsonArray([...,your_new_block])
  ```

- **Step 3: define a code generator**

code generator is needed to generate corresponding pluto code

```javascript

plutoGenerator.forBlock['initiate_and_confirm_activity'] = function(block, generator) {
  var activity_call = block.getFieldValue('value');
  var value_extra = generator.valueToCode(block, 'extra', ORDER.ATOMIC);

  var code = `initiate and confirm ${activity_call}`;
  return code;
};
```
place this code in `./src/generators/pluto.js`

In above example we are generating code for `initiate_and_confirm_activity` block.

[Blockly Developer Tools](https://blockly-demo.appspot.com/static/demos/blockfactory/index.html) also provide generator template code for created block.

[more info about code generation](https://developers.google.com/blockly/guides/create-custom-blocks/code-generation/overview)

- **Step 4: add block in toolbox**

To show block on workspace we have add the created block in toolbox category.
we can simply add created block in `./src/toolbox.js` using
```javascript
export const toolbox = {
    kind: "categoryToolbox",
    contents: [
        ...,
        {
            kind: "category",
            name: "Step main",
            colour:"30",
            // custom: "PROCEDURE",
            contents: [
                ...,
                {
                    kind: "block",
                    type: "initiate_and_confirm_activity"
                },
            ]
        }
}

```
[more info about toolbox](https://developers.google.com/blockly/guides/configure/web/toolbox)

references:
- [Block class](https://developers.google.com/blockly/reference/js/blockly.block_class)
- [CodeGenerator class](https://developers.google.com/blockly/reference/js/blockly.codegenerator_class)