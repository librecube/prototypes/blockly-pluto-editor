import * as Blockly from "blockly";
import { Colors } from "../../colors";
import { Types } from "../types";
import { all_step_declaration_types } from "./6_step_declaration";
import { all_step_preconditions_types } from "./7_step_preconditions";
import { all_step_main_types } from "./8_step_main";
import { all_step_watchdog_types } from "./9_step_watchdog";
import { all_step_confirmation_types } from "./10_step_confirmation";

export const all_procedure_main_types = [
  Types.ProcedureMainInitiateAndConfirmStep,
  Types.ProcedureMainInitiateAndConfirmActivity,
  Types.ProcedureMainInitiateActivity,
  Types.ProcedureMainInformUser,
  Types.ProcedureMainLog,
];

export const procedure_main_initiate_and_confirm_step = {
  type: "procedure_main_initiate_and_confirm_step",
  message0: "initiate and confirm step  %1 \n %2 \n %3",
  args0: [
    {
      type: "field_input",
      name: "step_name",
      text: "step_name",
    },
    {
      type: "input_value",
      name: "step_definition",
      check: [
        Types.ProcedureMainInitiateAndConfirmStepDefinitionPlaceholder,
        Types.ProcedureMainInitiateAndConfirmStepDefinition,
      ],
    },
    {
      type: "input_value",
      name: "continuation_test",
      check: Types.ProcedureMainInitiateAndConfirmStepContinuationTest,
    },
  ],
  previousStatement: Types.ProcedureMainInitiateAndConfirmStep,
  nextStatement: all_procedure_main_types,
  inputsInline: true,
  colour: Colors.BlockMain,
  tooltip: "Initiates and confirms the execution of a step.",
};

export const procedure_main_initiate_and_confirm_step_definition = {
  type: "procedure_main_initiate_and_confirm_step_definition",
  message0: "Step \n main %1 %2",
  args0: [
    {
      type: "input_dummy",
      name: "main_name",
    },
    {
      type: "input_statement",
      name: "main",
      check: all_step_main_types,
    },
  ],
  output: Types.ProcedureMainInitiateAndConfirmStepDefinition,
  colour: Colors.BlockMain,
  tooltip: "The definition of the step.",
  mutator: "initiate_confirm_step_definition_mutator",
};

export const procedure_main_optional_steps_sections_mutator = {
  type: "procedure_main_optional_steps_sections_mutator",
  message0:
    "declaration %1 %2 preconditions %3 %4 watchdog %5 %6 confirmation %7",
  args0: [
    {
      type: "field_checkbox",
      name: "declaration",
      checked: false,
    },
    {
      type: "input_dummy",
    },
    {
      type: "field_checkbox",
      name: "preconditions",
      checked: false,
    },
    {
      type: "input_dummy",
    },
    {
      type: "field_checkbox",
      name: "watchdog",
      checked: false,
    },
    {
      type: "input_dummy",
    },
    {
      type: "field_checkbox",
      name: "confirmation",
      checked: false,
    },
  ],
  colour: Colors.BlockMain,
};

Blockly.Extensions.registerMutator(
  "initiate_confirm_step_definition_mutator",
  {
    saveExtraState: function () {
      return {
        attachedSections: this.attachedSections_,
      };
    },
    loadExtraState: function (state) {
      this.attachedSections_ = state["attachedSections"];
      this.updateShape_();
    },
    decompose: function (workspace) {
      var topBlock = workspace.newBlock(
        "procedure_main_optional_steps_sections_mutator"
      );
      topBlock.initSvg();

      // Then we add one sub-block for each item in the list.
      this.attachedSections_?.includes("declaration") &&
        topBlock.setFieldValue("TRUE", "declaration");
      this.attachedSections_?.includes("preconditions") &&
        topBlock.setFieldValue("TRUE", "preconditions");
      this.attachedSections_?.includes("watchdog") &&
        topBlock.setFieldValue("TRUE", "watchdog");
      this.attachedSections_?.includes("confirmation") &&
        topBlock.setFieldValue("TRUE", "confirmation");

      return topBlock;
    },
    compose: function (topBlock) {
      var checkbox_declaration =
        topBlock.getFieldValue("declaration") === "TRUE";
      var checkbox_preconditions =
        topBlock.getFieldValue("preconditions") === "TRUE";
      var checkbox_watchdog = topBlock.getFieldValue("watchdog") === "TRUE";
      var checkbox_confirmation =
        topBlock.getFieldValue("confirmation") === "TRUE";

      // Then we collect up all of the connections of on our main block that are
      // referenced by our sub-blocks.
      // This relates to the saveConnections hook (explained below).
      var connections = [];
      if (checkbox_declaration) {
        connections.push("declaration");
      }
      if (checkbox_preconditions) {
        connections.push("preconditions");
      }
      if (checkbox_watchdog) {
        connections.push("watchdog");
      }
      if (checkbox_confirmation) {
        connections.push("confirmation");
      }

      // Then we disconnect any children where the sub-block associated with that
      // child has been deleted/removed from the stack. (pending)

      // Then we update the shape of our block (removing or adding inputs as necessary).
      // `this` refers to the main block.
      this.attachedSections_ = connections;
      this.updateShape_();
    },
    updateShape_: function () {
      if (!this.attachedSections_) {
        return;
      }
      const types = {
        declaration: all_step_declaration_types,
        preconditions: all_step_preconditions_types,
        main: all_step_main_types,
        watchdog: all_step_watchdog_types,
        confirmation: all_step_confirmation_types,
      };
      const sections = [
        "declaration",
        "preconditions",
        "main",
        "watchdog",
        "confirmation",
      ];
      const appendField = (name, statement_input, optional = true) => {
        if (!this.getInput(name) && !this.getInput(statement_input)) {
          this.appendDummyInput(name).appendField(
            `${statement_input} ${optional ? "" : ""}`
          );
          this.appendStatementInput(statement_input).setCheck(
            types[statement_input]
          );
        }
      };
      const deleteField = (name, statement_input) => {
        if (this.getInput(name) && this.getInput(statement_input)) {
          this.removeInput(name);
          this.removeInput(statement_input);
        }
      };
      // console.log("updating shape");

      sections.forEach((name, i, arr) => {
        var connections = [];
        var connection_values = [];
        if (this.attachedSections_?.includes(name)) {
          for (var k = i + 1; k < sections.length; k++) {
            if (this.getInput(sections[k])) {
              connections.push(
                this.getInput(sections[k]).connection?.targetConnection
              );
              connection_values.push(sections[k]);
              deleteField(`${sections[k]}_name`, sections[k]);
            }
          }
          appendField(`${name}_name`, name);
          // console.log(connection_values);
          connection_values.forEach((value, j, arr) => {
            if (value == "main") appendField(`${value}_name`, value, false);
            else appendField(`${value}_name`, value);
            connections[j]?.reconnect(this, value);
          });
        } else {
          if (name != "main") deleteField(`${name}_name`, name);
        }
      });
    },
  },
  () => {
    // console.log("mutator running....");
  },
  []
);

export const procedure_main_initiate_and_confirm_step_continuation_test = {
  type: "procedure_main_initiate_and_confirm_step_continuation_test",
  message0: "continuation test",
  output: Types.ProcedureMainInitiateAndConfirmStepContinuationTest,
  colour: Colors.BlockMain,
  mutator: "procedure_main_initiate_and_confirm_step_continuation_test_mutator",
};

export const procedure_main_initiate_and_confirm_step_continuation_test_parts_mutator =
{
  type: "procedure_main_initiate_and_confirm_step_continuation_test_parts_mutator",
  message0: "in case %1 %2",
  args0: [
    {
      type: "input_dummy",
    },
    {
      type: "input_statement",
      name: "CONTINUATION_TEST_STACK",
      check: [
        Types.ProcedureMainInitiateAndConfirmActivityConfirmed,
        Types.ProcedureMainInitiateAndConfirmActivityNotConfirmed,
        Types.ProcedureMainInitiateAndConfirmActivityAborted,
      ],
    },
  ],
  colour: Colors.BlockMain,
};

Blockly.Extensions.registerMutator(
  "procedure_main_initiate_and_confirm_step_continuation_test_mutator",
  {
    saveExtraState: function () {
      return {
        itemCountContinuationTest: this.itemCountContinuationTest_,
        ContinuationTest: this.ContinuationTest_,
      };
    },

    loadExtraState: function (state) {
      this.itemCountContinuationTest_ = state["itemCountContinuationTest"];
      this.ContinuationTest_ = state["ContinuationTest"];
      this.updateShape_();
    },

    decompose: function (workspace) {
      var topBlock = workspace.newBlock(
        "procedure_main_initiate_and_confirm_step_continuation_test_parts_mutator"
      );
      topBlock.initSvg();

      // for continuation test
      var connection = topBlock.getInput("CONTINUATION_TEST_STACK").connection;
      for (var i = 0; i < this.itemCountContinuationTest_; i++) {
        var child = workspace.newBlock(
          "procedure_main_initiate_and_confirm_activity_confirmed"
        );
        if (this.ContinuationTest_[i].name == "not confirmed") {
          child = workspace.newBlock(
            "procedure_main_initiate_and_confirm_activity_not_confirmed"
          );
        } else if (this.ContinuationTest_[i].name == "aborted") {
          child = workspace.newBlock(
            "procedure_main_initiate_and_confirm_activity_aborted"
          );
        }

        child.initSvg();
        connection.connect(child.previousConnection);
        connection = child.nextConnection;
      }

      return topBlock;
    },

    compose: function (topBlock) {
      var itemBlockContinuationTest = topBlock.getInputTargetBlock(
        "CONTINUATION_TEST_STACK"
      );

      var ContinuationTestConnections = [],
        ContinuationTestNames = [],
        ContinuationTestValues = [];

      while (
        itemBlockContinuationTest &&
        !itemBlockContinuationTest.isInsertionMarker()
      ) {
        ContinuationTestConnections.push(
          itemBlockContinuationTest.valueConnection_
        );
        ContinuationTestNames.push(
          itemBlockContinuationTest.type ==
            "procedure_main_initiate_and_confirm_activity_confirmed"
            ? "confirmed"
            : itemBlockContinuationTest.type ==
              "procedure_main_initiate_and_confirm_activity_not_confirmed"
              ? "not confirmed"
              : "aborted"
        );
        ContinuationTestValues.push(
          itemBlockContinuationTest.valueValue_ || ""
        );
        itemBlockContinuationTest =
          itemBlockContinuationTest.nextConnection &&
          itemBlockContinuationTest.nextConnection.targetBlock();
      }

      this.itemCountContinuationTest_ = ContinuationTestConnections.length;
      this.ContinuationTest_ = ContinuationTestNames.map((name, index) => ({
        name: name,
        value: ContinuationTestValues[index],
      }));

      this.updateShape_();

      for (var i = 0; i < this.itemCountContinuationTest_; i++) {
        if (ContinuationTestNames[i])
          this.setFieldValue(ContinuationTestNames[i], "CONTINUATION_NAME" + i);
        if (ContinuationTestValues[i])
          this.setFieldValue(
            ContinuationTestValues[i],
            "CONTINUATION_VALUE" + i
          );
        if (
          ContinuationTestValues[i] &&
          this.getInput("CONTINUATION" + i) &&
          this.getInput("CONTINUATION" + i).connection
        ) {
          ContinuationTestConnections[i]?.reconnect(this, "CONTINUATION" + i);
        }
      }
    },

    saveConnections: function (topBlock) {
      var itemBlockContinuationTest = topBlock.getInputTargetBlock(
        "CONTINUATION_TEST_STACK"
      );
      var j = 0;
      while (itemBlockContinuationTest) {
        var input = this.getInput("CONTINUATION" + j);
        itemBlockContinuationTest.valueConnection_ =
          input && input.connection && input.connection.targetConnection;
        itemBlockContinuationTest.valueValue_ = this.getFieldValue(
          `CONTINUATION_VALUE` + j
        );
        j++;
        itemBlockContinuationTest =
          itemBlockContinuationTest.nextConnection &&
          itemBlockContinuationTest.nextConnection.targetBlock();
      }
    },
    updateShape_: function () {
      for (var i = 0; this.getInput("END_ROW_CONTINUATION_TEST" + i); i++) {
        this.removeInput("END_ROW_CONTINUATION_TEST" + i);
      }
      for (var i = 0; this.getInput("CONTINUATION" + i); i++) {
        this.removeInput("CONTINUATION" + i);
      }

      // // update shape for continuations
      // if (!this.getInput("IN_CASE") && this.itemCountContinuationTest_ > 0) {
      //   this.appendEndRowInput("CONTINUATION_END_ROW");
      //   this.appendDummyInput("IN_CASE").appendField("case");
      //   this.appendEndRowInput("CONTINUATION_END_ROW_AFTER");
      // }

      // Add new inputs for continuations
      for (var i = 0; i < this.itemCountContinuationTest_; i++) {
        var input = this.appendDummyInput("CONTINUATION" + i)
          .appendField(this.ContinuationTest_[i].name, "CONTINUATION_NAME" + i)
          .appendField(":")
          .appendField(
            new Blockly.FieldDropdown([
              ["resume", "resume"],
              ["abort", "abort"],
              ["restart", "restart"],
              ["ask_user", "ask user"],
              ["continue", "continue"],
              ["terminate", "terminate"],
            ]),
            "CONTINUATION_VALUE" + i
          );
        this.appendEndRowInput("END_ROW_CONTINUATION_TEST" + i);
      }

      if (this.getInput("IN_CASE") && this.getInput("CONTINUATION0")) {
        this.moveInputBefore("CONTINUATION_END_ROW", "CONTINUATION0");
        this.moveInputBefore("IN_CASE", "CONTINUATION0");
        this.moveInputBefore("CONTINUATION_END_ROW_AFTER", "CONTINUATION0");
      }

      if (this.itemCountContinuationTest_ == 0) {
        this.getInput("IN_CASE") && this.removeInput("IN_CASE");
        this.getInput("END_ROW") && this.removeInput("END_ROW");
        this.getInput("CONTINUATION_END_ROW") &&
          this.removeInput("CONTINUATION_END_ROW");
        this.getInput("CONTINUATION_END_ROW_AFTER") &&
          this.removeInput("CONTINUATION_END_ROW_AFTER");
      }
    },
  },
  () => {
    // console.log("mutator running....",this)
  },
  [
    "procedure_main_initiate_and_confirm_activity_confirmed",
    "procedure_main_initiate_and_confirm_activity_not_confirmed",
    "procedure_main_initiate_and_confirm_activity_aborted",
  ]
);

export const procedure_main_initiate_and_confirm_activity = {
  type: "procedure_main_initiate_and_confirm_activity",
  message0: "initiate and confirm %1 \n %2 \n %3",
  args0: [
    {
      type: "field_input",
      name: "activity_call",
      text: "activity_call",
    },
    {
      type: "input_value",
      name: "refer_by",
      check: [Types.ProcedureMainInitiateActivityReferBy],
    },
    {
      type: "input_value",
      name: "continuation_test",
      check: Types.ProcedureMainInitiateAndConfirmStepContinuationTest,
    },
  ],
  inputsInline: true,
  previousStatement: Types.ProcedureMainInitiateAndConfirmActivity,
  nextStatement: all_procedure_main_types,
  colour: Colors.BlockMain,
  tooltip: "Initiate and confirm the execution of an activity.",
  mutator: "procedure_main_initiate_and_confirm_activity_mutator",
};

export const procedure_main_initiate_and_confirm_activity_parts_mutator = {
  type: "procedure_main_initiate_and_confirm_activity_parts_mutator",
  message0: "with arguments %1 %2 with directives %3 %4",
  args0: [
    {
      type: "input_dummy",
    },
    {
      type: "input_statement",
      name: "STACK",
      check: [Types.ProcedureMainInitiateActivityArgument],
    },
    {
      type: "input_dummy",
    },
    {
      type: "input_statement",
      name: "DIRECTIVE_STACK",
      check: [Types.ProcedureMainInitiateActivityDirective],
    },
  ],
  colour: Colors.BlockMain,
};

export const procedure_main_initiate_and_confirm_activity_confirmed = {
  type: "procedure_main_initiate_and_confirm_activity_confirmed",
  message0: "confirmed",
  previousStatement: Types.ProcedureMainInitiateAndConfirmActivityConfirmed,
  nextStatement: [
    Types.ProcedureMainInitiateAndConfirmActivityConfirmed,
    Types.ProcedureMainInitiateAndConfirmActivityNotConfirmed,
    Types.ProcedureMainInitiateAndConfirmActivityAborted,
  ],
  colour: Colors.BlockMain,
};

export const procedure_main_initiate_and_confirm_activity_not_confirmed = {
  type: "procedure_main_initiate_and_confirm_activity_not_confirmed",
  message0: "not confirmed",
  previousStatement: Types.ProcedureMainInitiateAndConfirmActivityNotConfirmed,
  nextStatement: [
    Types.ProcedureMainInitiateAndConfirmActivityConfirmed,
    Types.ProcedureMainInitiateAndConfirmActivityNotConfirmed,
    Types.ProcedureMainInitiateAndConfirmActivityAborted,
  ],
  colour: Colors.BlockMain,
};

export const procedure_main_initiate_and_confirm_activity_aborted = {
  type: "procedure_main_initiate_and_confirm_activity_aborted",
  message0: "aborted",
  previousStatement: Types.ProcedureMainInitiateAndConfirmActivityAborted,
  nextStatement: [
    Types.ProcedureMainInitiateAndConfirmActivityConfirmed,
    Types.ProcedureMainInitiateAndConfirmActivityNotConfirmed,
    Types.ProcedureMainInitiateAndConfirmActivityAborted,
  ],
  colour: Colors.BlockMain,
};

Blockly.Extensions.registerMutator(
  "procedure_main_initiate_and_confirm_activity_mutator",

  {
    saveExtraState: function () {
      return {
        itemCount: this.itemCount_,
        itemCountDirective: this.itemCountDirective_,
        arguments: this.arguments_,
        directives: this.directives_,
      };
    },

    loadExtraState: function (state) {
      this.itemCount_ = state["itemCount"];
      this.itemCountDirective_ = state["itemCountDirective"];
      this.arguments_ = state["arguments"];
      this.directives_ = state["directives"];
      this.updateShape_();
    },

    decompose: function (workspace) {
      var topBlock = workspace.newBlock(
        "procedure_main_initiate_and_confirm_activity_parts_mutator"
      );
      topBlock.initSvg();

      // for arguments
      var connection = topBlock.getInput("STACK").connection;
      for (var i = 0; i < this.itemCount_; i++) {
        var child = workspace.newBlock(
          "procedure_main_initiate_activity_argument"
        );
        child.initSvg();
        connection.connect(child.previousConnection);
        connection = child.nextConnection;
      }

      // for directives
      var connection = topBlock.getInput("DIRECTIVE_STACK").connection;
      for (var i = 0; i < this.itemCountDirective_; i++) {
        var child = workspace.newBlock(
          "procedure_main_initiate_activity_directive"
        );
        child.initSvg();
        connection.connect(child.previousConnection);
        connection = child.nextConnection;
      }

      return topBlock;
    },

    compose: function (topBlock) {
      var itemBlock = topBlock.getInputTargetBlock("STACK");
      var itemBlockDirective = topBlock.getInputTargetBlock("DIRECTIVE_STACK");

      var ArgConnections = [],
        ArgNames = [],
        ArgValues = [];
      var DirectiveConnections = [],
        DirectiveNames = [],
        DirectiveValues = [];

      while (itemBlock && !itemBlock.isInsertionMarker()) {
        ArgConnections.push(itemBlock.valueConnection_);
        ArgNames.push(itemBlock.nameValue_ || "");
        ArgValues.push(itemBlock.valueValue_ || "");
        itemBlock =
          itemBlock.nextConnection && itemBlock.nextConnection.targetBlock();
      }

      while (itemBlockDirective && !itemBlockDirective.isInsertionMarker()) {
        DirectiveConnections.push(itemBlockDirective.valueConnection_);
        DirectiveNames.push(itemBlockDirective.nameValue_ || "");
        DirectiveValues.push(itemBlockDirective.valueValue_ || "");
        itemBlockDirective =
          itemBlockDirective.nextConnection &&
          itemBlockDirective.nextConnection.targetBlock();
      }

      this.itemCount_ = ArgConnections.length;
      this.arguments_ = ArgNames.map((name, index) => ({
        name: name,
        value: ArgValues[index],
      }));

      this.itemCountDirective_ = DirectiveConnections.length;
      this.directives_ = DirectiveNames.map((name, index) => ({
        name: name,
        value: DirectiveValues[index],
      }));

      this.updateShape_();

      for (var i = 0; i < this.itemCount_; i++) {
        if (ArgNames[i]) this.setFieldValue(ArgNames[i], "ARG_NAME" + i);
        if (ArgValues[i]) this.setFieldValue(ArgValues[i], "ARG_VALUE" + i);
        if (
          ArgConnections[i] &&
          this.getInput("ARG" + i) &&
          this.getInput("ARG" + i).connection
        ) {
          ArgConnections[i]?.reconnect(this, "ARG" + i);
        }
      }

      for (var i = 0; i < this.itemCountDirective_; i++) {
        if (DirectiveNames[i])
          this.setFieldValue(DirectiveNames[i], "DIRECTIVE_NAME" + i);
        if (DirectiveValues[i])
          this.setFieldValue(DirectiveValues[i], "DIRECTIVE_VALUE" + i);
        if (
          DirectiveConnections[i] &&
          this.getInput("DIRECTIVE" + i) &&
          this.getInput("DIRECTIVE" + i).connection
        ) {
          DirectiveConnections[i]?.reconnect(this, "DIRECTIVE" + i);
        }
      }
    },

    saveConnections: function (topBlock) {
      function save_connection(block, name) {
        var itemBlock = topBlock.getInputTargetBlock(
          name == "ARG" ? "STACK" : "DIRECTIVE_STACK"
        );
        var i = 0;
        while (itemBlock) {
          var input = block.getInput(name + i);
          itemBlock.valueConnection_ =
            input && input.connection && input.connection.targetConnection;
          itemBlock.nameValue_ = block.getFieldValue(`${name}_NAME` + i);
          itemBlock.valueValue_ = block.getFieldValue(`${name}_VALUE` + i);
          i++;
          itemBlock =
            itemBlock.nextConnection && itemBlock.nextConnection.targetBlock();
        }
      }
      save_connection(this, "ARG");
      save_connection(this, "DIRECTIVE");
    },
    updateShape_: function () {
      // Remove existing argument inputs
      for (var i = 0; this.getInput("END_ROW" + i); i++) {
        this.removeInput("END_ROW" + i);
      }
      for (var i = 0; this.getInput("END_ROW_DIRECTIVE" + i); i++) {
        this.removeInput("END_ROW_DIRECTIVE" + i);
      }
      for (var i = 0; this.getInput("ARG" + i); i++) {
        this.removeInput("ARG" + i);
      }
      for (var i = 0; this.getInput("DIRECTIVE" + i); i++) {
        this.removeInput("DIRECTIVE" + i);
      }

      // update shape for arguments
      if (!this.getInput("WITH_ARG") && this.itemCount_ > 0) {
        this.appendEndRowInput("ARG_END_ROW");
        this.appendDummyInput("WITH_ARG").appendField("arguments");
        this.appendEndRowInput("ARG_END_ROW_AFTER");
      }

      // Add new inputs
      for (var i = 0; i < this.itemCount_; i++) {
        var input = this.appendDummyInput("ARG" + i)
          .appendField(
            new Blockly.FieldTextInput(
              this.arguments_[i] == ""
                ? this.arguments_[i].name
                : "argument_name"
            ),
            "ARG_NAME" + i
          )
          .appendField(":=")
          .appendField(
            new Blockly.FieldTextInput(
              this.arguments_[i] == ""
                ? this.arguments_[i].value
                : "argument_value"
            ),
            "ARG_VALUE" + i
          );
        this.appendEndRowInput("END_ROW" + i);
      }

      if (!this.getInput("WITH_DIRECTIVE") && this.itemCountDirective_ > 0) {
        this.appendEndRowInput("DIRECTIVE_END_ROW");
        this.appendDummyInput("WITH_DIRECTIVE").appendField("directives");
        this.appendEndRowInput("DIRECTIVE_END_ROW_AFTER");
      }
      // Add new inputs
      for (var i = 0; i < this.itemCountDirective_; i++) {
        var input = this.appendDummyInput("DIRECTIVE" + i)
          .appendField(
            new Blockly.FieldTextInput(
              this.directives_[i] == ""
                ? this.directives_[i].name
                : "directive_name"
            ),
            "DIRECTIVE_NAME" + i
          )
          .appendField(":=")
          .appendField(
            new Blockly.FieldTextInput(
              this.directives_[i] == ""
                ? this.directives_[i].value
                : "directive_value"
            ),
            "DIRECTIVE_VALUE" + i
          );
        this.appendEndRowInput("END_ROW_DIRECTIVE" + i);
      }

      if (this.getInput("WITH_DIRECTIVE") && this.getInput("DIRECTIVE0")) {
        this.moveInputBefore("DIRECTIVE_END_ROW", "DIRECTIVE0");
        this.moveInputBefore("WITH_DIRECTIVE", "DIRECTIVE0");
        this.moveInputBefore("DIRECTIVE_END_ROW_AFTER", "DIRECTIVE0");
      }

      if (this.itemCount_ == 0) {
        this.getInput("ARG_END_ROW") && this.removeInput("ARG_END_ROW");
        this.getInput("ARG_END_ROW_AFTER") &&
          this.removeInput("ARG_END_ROW_AFTER");
        this.getInput("WITH_ARG") && this.removeInput("WITH_ARG");
        this.getInput("END_ROW") && this.removeInput("END_ROW");
      } else {
        this.getInput("END_ROW" + (this.itemCount_ - 1)) &&
          this.removeInput("END_ROW" + (this.itemCount_ - 1));
      }
      if (this.itemCountDirective_ == 0) {
        this.getInput("WITH_DIRECTIVE") && this.removeInput("WITH_DIRECTIVE");
        this.getInput("END_ROW") && this.removeInput("END_ROW");
        this.getInput("DIRECTIVE_END_ROW") &&
          this.removeInput("DIRECTIVE_END_ROW");
        this.getInput("DIRECTIVE_END_ROW_AFTER") &&
          this.removeInput("DIRECTIVE_END_ROW_AFTER");
      } else {
        this.getInput("END_ROW_DIRECTIVE" + (this.itemCountDirective_ - 1)) &&
          this.removeInput(
            "END_ROW_DIRECTIVE" + (this.itemCountDirective_ - 1)
          );
      }

      // if (this.getInput("continuation_test") && this.getInput("refer_by")) {
      //   this.moveInputBefore("continuation_test", null);
      //   this.moveInputBefore("refer_by", null);
      // }
    },
  },

  () => {
    // console.log("mutator running....");
  },

  [
    "procedure_main_initiate_activity_argument",
    "procedure_main_initiate_activity_directive",
  ]
);

export const procedure_main_initiate_activity = {
  type: "procedure_main_initiate_activity",
  message0: "initiate %1 %2",
  args0: [
    {
      type: "field_input",
      name: "activity_call",
      text: "activity_call",
    },
    {
      type: "input_value",
      name: "refer_by",
      check: [Types.ProcedureMainInitiateActivityReferBy],
    },
  ],
  inputsInline: true,
  previousStatement: Types.ProcedureMainInitiateActivity,
  nextStatement: all_procedure_main_types,
  colour: Colors.BlockMain,
  tooltip: "Inititates the execution of an activity. The initiated activity runs in parallel with the initiating procedure.",
  mutator: "procedure_main_initiate_activity_mutator",
};

export const procedure_main_initiate_activity_parts_mutator = {
  type: "procedure_main_initiate_activity_parts_mutator",
  message0: "with arguments %1 %2 with directives %3 %4",
  args0: [
    {
      type: "input_dummy",
    },
    {
      type: "input_statement",
      name: "STACK",
      check: [Types.ProcedureMainInitiateActivityArgument],
    },
    {
      type: "input_dummy",
    },
    {
      type: "input_statement",
      name: "DIRECTIVE_STACK",
      check: [Types.ProcedureMainInitiateActivityDirective],
    },
  ],
  colour: Colors.BlockMain,
};

export const procedure_main_initiate_activity_argument = {
  type: "procedure_main_initiate_activity_argument",
  message0: "argument",
  previousStatement: Types.ProcedureMainInitiateActivityArgument,
  nextStatement: [Types.ProcedureMainInitiateActivityArgument],
  colour: Colors.BlockMain,
};

export const procedure_main_initiate_activity_directive = {
  type: "procedure_main_initiate_activity_directive",
  message0: "directive",
  previousStatement: [Types.ProcedureMainInitiateActivityDirective],
  nextStatement: [Types.ProcedureMainInitiateActivityDirective],
  colour: Colors.BlockMain,
};

Blockly.Extensions.registerMutator(
  "procedure_main_initiate_activity_mutator",

  {
    saveExtraState: function () {
      return {
        itemCount: this.itemCount_,
        itemCountDirective: this.itemCountDirective_,
        arguments: this.arguments_,
        directives: this.directives_,
      };
    },

    loadExtraState: function (state) {
      this.itemCount_ = state["itemCount"];
      this.itemCountDirective_ = state["itemCountDirective"];
      this.arguments_ = state["arguments"];
      this.directives_ = state["directives"];
      this.updateShape_();
    },

    decompose: function (workspace) {
      var topBlock = workspace.newBlock(
        "procedure_main_initiate_activity_parts_mutator"
      );
      topBlock.initSvg();

      // for arguments
      var connection = topBlock.getInput("STACK").connection;
      for (var i = 0; i < this.itemCount_; i++) {
        var child = workspace.newBlock(
          "procedure_main_initiate_activity_argument"
        );
        child.initSvg();
        connection.connect(child.previousConnection);
        connection = child.nextConnection;
      }

      // for directives
      var connection = topBlock.getInput("DIRECTIVE_STACK").connection;
      for (var i = 0; i < this.itemCountDirective_; i++) {
        var child = workspace.newBlock(
          "procedure_main_initiate_activity_directive"
        );
        child.initSvg();
        connection.connect(child.previousConnection);
        connection = child.nextConnection;
      }

      return topBlock;
    },

    compose: function (topBlock) {
      var itemBlock = topBlock.getInputTargetBlock("STACK");
      var itemBlockDirective = topBlock.getInputTargetBlock("DIRECTIVE_STACK");
      var ArgConnections = [],
        ArgNames = [],
        ArgValues = [];
      var DirectiveConnections = [],
        DirectiveNames = [],
        DirectiveValues = [];

      while (itemBlock && !itemBlock.isInsertionMarker()) {
        ArgConnections.push(itemBlock.valueConnection_);
        ArgNames.push(itemBlock.nameValue_ || "");
        ArgValues.push(itemBlock.valueValue_ || "");
        itemBlock =
          itemBlock.nextConnection && itemBlock.nextConnection.targetBlock();
      }
      while (itemBlockDirective && !itemBlockDirective.isInsertionMarker()) {
        DirectiveConnections.push(itemBlockDirective.valueConnection_);
        DirectiveNames.push(itemBlockDirective.nameValue_ || "");
        DirectiveValues.push(itemBlockDirective.valueValue_ || "");
        itemBlockDirective =
          itemBlockDirective.nextConnection &&
          itemBlockDirective.nextConnection.targetBlock();
      }

      this.itemCount_ = ArgConnections.length;
      this.arguments_ = ArgNames.map((name, index) => ({
        name: name,
        value: ArgValues[index],
      }));
      this.itemCountDirective_ = DirectiveConnections.length;
      this.directives_ = DirectiveNames.map((name, index) => ({
        name: name,
        value: DirectiveValues[index],
      }));
      this.updateShape_();
      for (var i = 0; i < this.itemCount_; i++) {
        if (ArgNames[i]) this.setFieldValue(ArgNames[i], "ARG_NAME" + i);
        if (ArgValues[i]) this.setFieldValue(ArgValues[i], "ARG_VALUE" + i);
        if (
          ArgConnections[i] &&
          this.getInput("ARG" + i) &&
          this.getInput("ARG" + i).connection
        ) {
          ArgConnections[i]?.reconnect(this, "ARG" + i);
        }
      }
      for (var i = 0; i < this.itemCountDirective_; i++) {
        if (DirectiveNames[i])
          this.setFieldValue(DirectiveNames[i], "DIRECTIVE_NAME" + i);
        if (DirectiveValues[i])
          this.setFieldValue(DirectiveValues[i], "DIRECTIVE_VALUE" + i);
        if (
          DirectiveConnections[i] &&
          this.getInput("DIRECTIVE" + i) &&
          this.getInput("DIRECTIVE" + i).connection
        ) {
          DirectiveConnections[i]?.reconnect(this, "DIRECTIVE" + i);
        }
      }
    },

    saveConnections: function (topBlock) {
      function save_connection(block, name) {
        var itemBlock = topBlock.getInputTargetBlock(
          name == "ARG" ? "STACK" : "DIRECTIVE_STACK"
        );
        var i = 0;
        while (itemBlock) {
          var input = block.getInput(name + i);
          itemBlock.valueConnection_ =
            input && input.connection && input.connection.targetConnection;
          itemBlock.nameValue_ = block.getFieldValue(`${name}_NAME` + i);
          itemBlock.valueValue_ = block.getFieldValue(`${name}_VALUE` + i);
          i++;
          itemBlock =
            itemBlock.nextConnection && itemBlock.nextConnection.targetBlock();
        }
      }
      save_connection(this, "ARG");
      save_connection(this, "DIRECTIVE");
    },
    updateShape_: function () {
      // Remove existing argument inputs
      for (var i = 0; this.getInput("END_ROW" + i); i++) {
        this.removeInput("END_ROW" + i);
      }
      for (var i = 0; this.getInput("END_ROW_DIRECTIVE" + i); i++) {
        this.removeInput("END_ROW_DIRECTIVE" + i);
      }
      for (var i = 0; this.getInput("ARG" + i); i++) {
        this.removeInput("ARG" + i);
      }
      for (var i = 0; this.getInput("DIRECTIVE" + i); i++) {
        this.removeInput("DIRECTIVE" + i);
      }

      // update shape for arguments
      if (!this.getInput("WITH_ARG") && this.itemCount_ > 0) {
        this.appendEndRowInput("ARG_END_ROW");
        this.appendDummyInput("WITH_ARG").appendField("with arguments");
        this.appendEndRowInput("ARG_END_ROW_AFTER");
      }

      // Add new inputs
      for (var i = 0; i < this.itemCount_; i++) {
        var input = this.appendDummyInput("ARG" + i)
          .appendField(
            new Blockly.FieldTextInput(
              this.arguments_[i] == ""
                ? this.arguments_[i].name
                : "argument_name"
            ),
            "ARG_NAME" + i
          )
          .appendField(":=")
          .appendField(
            new Blockly.FieldTextInput(
              this.arguments_[i] == ""
                ? this.arguments_[i].value
                : "argument_value"
            ),
            "ARG_VALUE" + i
          );
        this.appendEndRowInput("END_ROW" + i);
      }

      if (!this.getInput("WITH_DIRECTIVE") && this.itemCountDirective_ > 0) {
        this.appendEndRowInput("DIRECTIVE_END_ROW");
        this.appendDummyInput("WITH_DIRECTIVE").appendField("with directives");
        this.appendEndRowInput("DIRECTIVE_END_ROW_AFTER");
      }
      // Add new inputs
      for (var i = 0; i < this.itemCountDirective_; i++) {
        var input = this.appendDummyInput("DIRECTIVE" + i)
          .appendField(
            new Blockly.FieldTextInput(
              this.directives_[i] == ""
                ? this.directives_[i].name
                : "directive_name"
            ),
            "DIRECTIVE_NAME" + i
          )
          .appendField(":=")
          .appendField(
            new Blockly.FieldTextInput(
              this.directives_[i] == ""
                ? this.directives_[i].value
                : "directive_value"
            ),
            "DIRECTIVE_VALUE" + i
          );
        this.appendEndRowInput("END_ROW_DIRECTIVE" + i);
      }

      if (this.getInput("WITH_DIRECTIVE") && this.getInput("DIRECTIVE0")) {
        this.moveInputBefore("DIRECTIVE_END_ROW", "DIRECTIVE0");
        this.moveInputBefore("WITH_DIRECTIVE", "DIRECTIVE0");
        this.moveInputBefore("DIRECTIVE_END_ROW_AFTER", "DIRECTIVE0");
      }

      if (this.itemCount_ == 0) {
        this.getInput("ARG_END_ROW") && this.removeInput("ARG_END_ROW");
        this.getInput("ARG_END_ROW_AFTER") &&
          this.removeInput("ARG_END_ROW_AFTER");
        this.getInput("WITH_ARG") && this.removeInput("WITH_ARG");
        this.getInput("END_ROW") && this.removeInput("END_ROW");
      } else {
        this.getInput("END_ROW" + (this.itemCount_ - 1)) &&
          this.removeInput("END_ROW" + (this.itemCount_ - 1));
      }
      if (this.itemCountDirective_ == 0) {
        this.getInput("WITH_DIRECTIVE") && this.removeInput("WITH_DIRECTIVE");
        this.getInput("END_ROW") && this.removeInput("END_ROW");
        this.getInput("DIRECTIVE_END_ROW") &&
          this.removeInput("DIRECTIVE_END_ROW");
        this.getInput("DIRECTIVE_END_ROW_AFTER") &&
          this.removeInput("DIRECTIVE_END_ROW_AFTER");
      }
    },
  },

  () => {
    // console.log("mutator running....");
  },

  [
    "procedure_main_initiate_activity_argument",
    "procedure_main_initiate_activity_directive",
  ]
);

export const procedure_main_initiate_activity_refer_by = {
  type: "procedure_main_initiate_activity_refer_by",
  message0: "refer by %1",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "activity_reference",
    },
  ],
  output: Types.ProcedureMainInitiateActivityReferBy,
  colour: Colors.BlockMain,
};

export const procedure_main_inform_user = {
  type: "procedure_main_inform_user",
  message0: "inform user %1",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "text",
    },
  ],
  previousStatement: Types.ProcedureMainInformUser,
  nextStatement: all_procedure_main_types,
  colour: Colors.BlockMain,
  tooltip: "Output a message for acknowledgement by the user.",
};

export const procedure_main_log = {
  type: "procedure_main_log",
  message0: "log %1",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "text",
    },
  ],
  previousStatement: Types.ProcedureMainLog,
  nextStatement: all_procedure_main_types,
  colour: Colors.BlockMain,
  tooltip: "Log a message to the procedure execution log.",
};
