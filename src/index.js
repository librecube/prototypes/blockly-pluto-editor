import * as Blockly from "blockly";
import { blocks } from "./blocks/procedure";
import { plutoGenerator } from "./generators/pluto";
import { save, load } from "./serialization";
import { toolbox } from "./toolbox";
import "./index.css";

// Register the blocks and generator with Blockly
Blockly.common.defineBlocks(blocks);

// Set up UI elements and inject Blockly
const codeDiv = document.getElementById("generatedCode").firstChild;
// const outputDiv = document.getElementById("output");
const blocklyDiv = document.getElementById("blocklyDiv");
const ws = Blockly.inject(blocklyDiv, {
  toolbox,
  // uncomment if we need zoom controls
  zoom: {
    controls: true,
    wheel: true,
    startScale: 1.0,
    maxScale: 3,
    minScale: 0.1,
    scaleSpeed: 1.05,
    pinch: true,
  },
  // grid
  grid: {
    spacing: 20,
    length: 3,
    colour: "#ccc",
    snap: true,
  },
  // theme
  //renderer: 'zelos'
});

var xmlText =
  '<xml xmlns="https://developers.google.com/blockly/xml">' +
  '<block type="procedure" id="Z|sR|@Cl@3nHBAyk]}x#" deletable="false" movable="false" x="200" y="180"></block></xml>';
Blockly.Xml.domToWorkspace(Blockly.utils.xml.textToDom(xmlText), ws);

// blocks color Saturation
Blockly.utils.colour.setHsvSaturation(0.50); // 0 (inclusive) to 1 (exclusive), defaulting to 0.45
Blockly.utils.colour.setHsvValue(0.75); // 0 (inclusive) to 1 (exclusive), defaulting to 0.65

// This function resets the code and output divs, shows the
// generated code from the workspace, and evals the code.
// In a real application, you probably shouldn't use `eval`.
const runCode = () => {
  const code = plutoGenerator.workspaceToCode(ws);
  codeDiv.innerText = code;
  // outputDiv.innerHTML = "";
  // eval(code);
};

// Load the initial state from storage and run the code.
load(ws);
runCode();

// Every time the workspace changes state, save the changes to storage.
ws.addChangeListener((e) => {
  // UI events are things like scrolling, zooming, etc.
  // No need to save after one of these.
  if (e.isUiEvent) return;
  save(ws);
});

// Whenever the workspace changes meaningfully, run the code again.
ws.addChangeListener((e) => {
  // Don't run the code when the workspace finishes loading; we're
  // already running it once when the application starts.
  // Don't run the code during drags; we might have invalid state.
  // if(!e.isUiEvent) console.log("event:"+e.type +" "+JSON.stringify(e.toJson()))
  if (
    e.isUiEvent ||
    e.type == Blockly.Events.FINISHED_LOADING ||
    ws.isDragging()
  ) {
    return;
  }
  if (e.type === Blockly.Events.BLOCK_MOVE) {
    // console.log("running change listener")
    const block = ws.getBlockById(e.blockId);
    // console.log(block.type)
    if (block != null) {
      if (!block.isEnabled() && block.getParent() != null) {
        // console.log("running")
        block.setEnabled(true);
      }
    }
  }
  runCode();
});
