import * as Blockly from "blockly";
import { Colors } from "../../colors";
import { Types } from "../types";

export const all_step_watchdog_types = [
  Types.StepWatchdogInitiateAndConfirmStep,
];

export const step_watchdog_initiate_and_confirm_step = {
  type: "step_watchdog_initiate_and_confirm_step",
  message0: "initiate and confirm step  %1 \n %2 \n %3",
  args0: [
    {
      type: "field_input",
      name: "step_name",
      text: "step_name",
    },
    {
      type: "input_value",
      name: "step_definition",
      check: [
        Types.ProcedureMainInitiateAndConfirmStepDefinitionPlaceholder,
        Types.ProcedureWatchdogInitiateAndConfirmStepDefinition,
      ],
    },
    {
      type: "input_value",
      name: "continuation_test",
      check: Types.ProcedureMainInitiateAndConfirmStepContinuationTest,
    },
  ],
  previousStatement: Types.StepWatchdogInitiateAndConfirmStep,
  nextStatement: [Types.StepWatchdogInitiateAndConfirmStep],
  inputsInline: true,
  colour: Colors.BlockWatchdog,
  tooltip:
    "Initiate and confirm execution of the steps that compose the watchdog body (called watchdog steps). All steps are executed in parallel.",
};