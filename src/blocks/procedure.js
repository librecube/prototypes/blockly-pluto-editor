import * as Blockly from "blockly";
import { Colors } from "../colors";
import { Types } from "./types";

import { all_procedure_preconditions_types } from "./sections/2_procedure_preconditions";
import { all_procedure_main_types } from "./sections/3_procedure_main";
import { all_procedure_confirmation_types } from "./sections/5_procedure_confirmation";
import { all_procedure_watchdog_types } from "./sections/4_procedure_watchdog";

import {
  procedure_declaration_declare_event,
  procedure_declaration_described_by,
} from "./sections/1_procedure_declaration";

import {
  procedure_preconditions_wait_until_absolute_time,
  procedure_preconditions_wait_for_relative_time,
  procedure_preconditions_wait_until_true,
  procedure_preconditions_if_condition_true,
  procedure_preconditions_relational_expression,
  procedure_preconditions_timeout,
  procedure_preconditions_raise_event,
} from "./sections/2_procedure_preconditions";

import {
  procedure_main_optional_steps_sections_mutator,
  procedure_main_inform_user,
  procedure_main_initiate_and_confirm_step,
  procedure_main_initiate_and_confirm_step_definition,
  procedure_main_initiate_and_confirm_step_continuation_test_parts_mutator,
  procedure_main_initiate_and_confirm_step_continuation_test,
  procedure_main_initiate_activity,
  procedure_main_initiate_activity_refer_by,
  procedure_main_initiate_activity_argument,
  procedure_main_initiate_activity_directive,
  procedure_main_initiate_activity_parts_mutator,
  procedure_main_log,
  procedure_main_initiate_and_confirm_activity,
  procedure_main_initiate_and_confirm_activity_parts_mutator,
  procedure_main_initiate_and_confirm_activity_confirmed,
  procedure_main_initiate_and_confirm_activity_not_confirmed,
  procedure_main_initiate_and_confirm_activity_aborted,
} from "./sections/3_procedure_main";

import {
  procedure_watchdog_initiate_and_confirm_step,
  procedure_watchdog_optional_steps_sections_mutator,
  procedure_watchdog_initiate_and_confirm_step_definition,
  procedure_watchdog_initiate_and_confirm_step_continuation_test,
} from "./sections/4_procedure_watchdog";

import {
  procedure_confirmation_wait_until_absolute_time,
  procedure_confirmation_wait_for_relative_time,
  procedure_confirmation_wait_until_true,
  procedure_confirmation_if_condition_true,
  procedure_confirmation_relational_expression,
  procedure_confirmation_timeout,
  procedure_confirmation_raise_event,
} from "./sections/5_procedure_confirmation";

import {
  step_declaration_declare_variable,
  step_declaration_declare_event,
  step_declaration_described_by,
} from "./sections/6_step_declaration";

import {
  step_preconditions_wait_until_absolute_time,
  step_preconditions_wait_for_relative_time,
  step_preconditions_wait_until_true,
  step_preconditions_if_condition_true,
  step_preconditions_timeout,
  step_preconditions_raise_event,
  step_preconditions_relational_expression,
} from "./sections/7_step_preconditions";

import {
  step_main_set_context,
  step_main_assignment_statement,
  step_main_logic_if,
  step_main_logic_if_optional_else_mutator,
  step_main_logic_expression,
  step_main_logic_repeat_statement,
  step_main_logic_case,
  step_main_logic_case_optional_parts_mutator,
  step_main_logic_case_or_is_block,
  step_main_logic_while,
  step_main_logic_for_statement,
  step_main_wait_until_absolute_time,
  step_main_wait_for_relative_time,
  step_main_wait_for_event,
  step_main_wait_until_true,
  step_main_wait_timeout,
  step_main_raise_event,
  step_main_initiate_and_confirm_step,
  step_main_initiate_and_confirm_activity,
  step_main_initiate_activity,
  step_main_inform_user,
  step_main_log,
} from "./sections/8_step_main";

import { step_watchdog_initiate_and_confirm_step } from "./sections/9_step_watchdog";

import {
  step_confirmation_wait_until_absolute_time,
  step_confirmation_wait_for_relative_time,
  step_confirmation_wait_until_true,
  step_confirmation_if_condition_true,
  step_confirmation_relational_expression,
  step_confirmation_timeout,
  step_confirmation_raise_event,
} from "./sections/10_step_confirmation";

import {
  step_definition_placeholder,
  continuation_test_placeholder,
  refer_by_placeholder,
  described_by_placeholder,
  expression_placeholder,
  timeout_placeholder,
  raise_event_placeholder,
} from "./sections/placeholders";

const procedure = {
  type: "procedure",
  message0: "Procedure \n main %1 %2",
  args0: [
    {
      type: "input_end_row",
      name: "main_name",
    },
    {
      type: "input_statement",
      name: "main",
      check: all_procedure_main_types,
    },
  ],
  inputsInline: false,
  colour: Colors.CategoryProcedure,
  mutator: "procedure_mutator",
};

export const procedure_optional_sections_mutator = {
  type: "procedure_optional_sections_mutator",
  message0:
    "declaration %1 %2 preconditions %3 %4 watchdog %5 %6 confirmation %7",
  args0: [
    {
      type: "field_checkbox",
      name: "declaration",
      checked: false,
    },
    {
      type: "input_dummy",
    },
    {
      type: "field_checkbox",
      name: "preconditions",
      checked: false,
    },
    {
      type: "input_dummy",
    },
    {
      type: "field_checkbox",
      name: "watchdog",
      checked: false,
    },
    {
      type: "input_dummy",
    },
    {
      type: "field_checkbox",
      name: "confirmation",
      checked: false,
    },
  ],
  colour: Colors.CategoryProcedure,
};

Blockly.Extensions.registerMutator(
  "procedure_mutator",
  {
    saveExtraState: function () {
      return {
        attachedSections: this.attachedSections_,
      };
    },
    loadExtraState: function (state) {
      this.attachedSections_ = state["attachedSections"];
      this.updateShape_();
    },
    decompose: function (workspace) {
      var topBlock = workspace.newBlock("procedure_optional_sections_mutator");
      topBlock.initSvg();

      // Then we add one sub-block for each item in the list.
      this.attachedSections_?.includes("declaration") &&
        topBlock.setFieldValue("TRUE", "declaration");
      this.attachedSections_?.includes("preconditions") &&
        topBlock.setFieldValue("TRUE", "preconditions");
      this.attachedSections_?.includes("watchdog") &&
        topBlock.setFieldValue("TRUE", "watchdog");
      this.attachedSections_?.includes("confirmation") &&
        topBlock.setFieldValue("TRUE", "confirmation");

      return topBlock;
    },
    compose: function (topBlock) {
      var checkbox_declaration =
        topBlock.getFieldValue("declaration") === "TRUE";
      var checkbox_preconditions =
        topBlock.getFieldValue("preconditions") === "TRUE";
      var checkbox_watchdog = topBlock.getFieldValue("watchdog") === "TRUE";
      var checkbox_confirmation =
        topBlock.getFieldValue("confirmation") === "TRUE";

      // Then we collect up all of the connections of on our main block that are
      // referenced by our sub-blocks.
      // This relates to the saveConnections hook (explained below).
      var connections = [];
      if (checkbox_declaration) {
        connections.push("declaration");
      }
      if (checkbox_preconditions) {
        connections.push("preconditions");
      }
      if (checkbox_watchdog) {
        connections.push("watchdog");
      }
      if (checkbox_confirmation) {
        connections.push("confirmation");
      }

      // Then we disconnect any children where the sub-block associated with that
      // child has been deleted/removed from the stack. (pending)

      // Then we update the shape of our block (removing or adding inputs as necessary).
      // `this` refers to the main block.
      this.attachedSections_ = connections;
      this.updateShape_();

      // And finally we reconnect any child blocks. (pending)
    },
    saveConnections: function (topBlock) {
      // First we get the first sub-block (which represents an input on our main block).
      // var itemBlock = topBlock.getInputTargetBlock("statements");
      // Then we go through and assign references to connections on our main block
      // (input.connection.targetConnection) to properties on our sub blocks
      // (itemBlock.valueConnection_).
    },
    updateShape_: function () {
      if (!this.attachedSections_) {
        return;
      }
      const types = {
        declaration: [Types.ProcedureDeclarationDeclareEvent],
        preconditions: all_procedure_preconditions_types,
        main: all_procedure_main_types,
        watchdog: all_procedure_watchdog_types,
        confirmation: all_procedure_confirmation_types,
      };
      const sections = [
        "declaration",
        "preconditions",
        "main",
        "watchdog",
        "confirmation",
      ];
      const appendField = (name, statement_input, optional = true) => {
        if (!this.getInput(name) && !this.getInput(statement_input)) {
          this.appendDummyInput(name).appendField(
            `${statement_input} ${optional ? "" : ""}`
          );
          this.appendStatementInput(statement_input).setCheck(
            types[statement_input]
          );
        }
      };
      const deleteField = (name, statement_input) => {
        if (this.getInput(name) && this.getInput(statement_input)) {
          this.removeInput(name);
          this.removeInput(statement_input);
        }
      };

      sections.forEach((name, i, arr) => {
        var connections = [];
        var connection_values = [];
        if (this.attachedSections_?.includes(name)) {
          for (var k = i + 1; k < sections.length; k++) {
            if (this.getInput(sections[k])) {
              connections.push(
                this.getInput(sections[k]).connection?.targetConnection
              );
              connection_values.push(sections[k]);
              deleteField(`${sections[k]}_name`, sections[k]);
            }
          }
          appendField(`${name}_name`, name);
          connection_values.forEach((value, j, arr) => {
            if (value == "main") appendField(`${value}_name`, value, false);
            else appendField(`${value}_name`, value);
            connections[j]?.reconnect(this, value);
          });
        } else {
          if (name != "main") deleteField(`${name}_name`, name);
        }
      });
    },
  },
  () => {
    //console.log("");
  },
  []
);

export const blocks = Blockly.common.createBlockDefinitionsFromJsonArray([
  procedure,
  procedure_optional_sections_mutator,

  procedure_declaration_declare_event,
  procedure_declaration_described_by,

  procedure_preconditions_wait_until_absolute_time,
  procedure_preconditions_wait_for_relative_time,
  procedure_preconditions_wait_until_true,
  procedure_preconditions_if_condition_true,
  procedure_preconditions_relational_expression,
  procedure_preconditions_timeout,
  procedure_preconditions_raise_event,

  procedure_main_optional_steps_sections_mutator,
  procedure_main_initiate_and_confirm_step,
  procedure_main_initiate_and_confirm_step_definition,
  procedure_main_initiate_and_confirm_step_continuation_test,
  procedure_main_initiate_and_confirm_step_continuation_test_parts_mutator,
  procedure_main_initiate_and_confirm_activity,
  procedure_main_initiate_and_confirm_activity_parts_mutator,
  procedure_main_initiate_and_confirm_activity_confirmed,
  procedure_main_initiate_and_confirm_activity_not_confirmed,
  procedure_main_initiate_and_confirm_activity_aborted,
  procedure_main_initiate_activity_refer_by,
  procedure_main_initiate_activity,
  procedure_main_initiate_activity_parts_mutator,
  procedure_main_initiate_activity_argument,
  procedure_main_initiate_activity_directive,
  procedure_main_inform_user,
  procedure_main_log,

  procedure_watchdog_initiate_and_confirm_step,
  procedure_watchdog_initiate_and_confirm_step_definition,
  procedure_watchdog_optional_steps_sections_mutator,
  procedure_watchdog_initiate_and_confirm_step_continuation_test,

  procedure_confirmation_wait_until_absolute_time,
  procedure_confirmation_wait_for_relative_time,
  procedure_confirmation_wait_until_true,
  procedure_confirmation_if_condition_true,
  procedure_confirmation_relational_expression,
  procedure_confirmation_timeout,
  procedure_confirmation_raise_event,

  step_declaration_declare_variable,
  step_declaration_declare_event,
  step_declaration_described_by,

  step_preconditions_wait_until_absolute_time,
  step_preconditions_wait_for_relative_time,
  step_preconditions_wait_until_true,
  step_preconditions_if_condition_true,
  step_preconditions_timeout,
  step_preconditions_raise_event,
  step_preconditions_relational_expression,

  step_main_set_context,
  step_main_assignment_statement,
  step_main_logic_if,
  step_main_logic_if_optional_else_mutator,
  step_main_logic_expression,
  step_main_logic_repeat_statement,
  step_main_logic_case,
  step_main_logic_case_optional_parts_mutator,
  step_main_logic_case_or_is_block,
  step_main_logic_while,
  step_main_logic_for_statement,
  step_main_wait_until_absolute_time,
  step_main_wait_for_relative_time,
  step_main_wait_for_event,
  step_main_wait_until_true,
  step_main_wait_timeout,
  step_main_raise_event,
  step_main_initiate_and_confirm_step,
  step_main_initiate_and_confirm_activity,
  step_main_initiate_activity,
  step_main_inform_user,
  step_main_log,

  step_watchdog_initiate_and_confirm_step,

  step_confirmation_wait_until_absolute_time,
  step_confirmation_wait_for_relative_time,
  step_confirmation_wait_until_true,
  step_confirmation_if_condition_true,
  step_confirmation_relational_expression,
  step_confirmation_timeout,
  step_confirmation_raise_event,
  
  step_definition_placeholder,
  continuation_test_placeholder,
  refer_by_placeholder,
  described_by_placeholder,
  expression_placeholder,
  timeout_placeholder,
  raise_event_placeholder,
]);
