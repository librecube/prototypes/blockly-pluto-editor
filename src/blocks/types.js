export const Types = {
  // common
  None: "None",
  Any: null,

  // procedure declaration
  ProcedureDeclarationDeclareEvent: "ProcedureDeclarationDeclareEvent",
  ProcedureDeclarationDescribedBy: "ProcedureDeclarationDescribedBy",

  // procedure preconditions
  ProcedurePreconditionsWaitUntilAbsoluteTime:
    "ProcedurePreconditionsWaitUntilAbsoluteTime",
  ProcedurePreconditionsWaitForRelativeTime:
    "ProcedurePreconditionsWaitForRelativeTime",
  ProcedurePreconditionsWaitUntilTrue: "ProcedurePreconditionsWaitUntilTrue",
  ProcedurePreconditionsIfConditionTrue:
    "ProcedurePreconditionsIfConditionTrue",
  ProcedurePreconditionsTimeout: "ProcedurePreconditionsTimeout",
  ProcedurePreconditionsExpression: "ProcedurePreconditionsExpression",
  ProcedurePreconditionsExpressionValue:
    "ProcedurePreconditionsExpressionValue",
  ProcedurePreconditionsTimeInterval: "ProcedurePreconditionsTimeInterval",
  ProcedurePreconditionsRaiseEvent: "ProcedurePreconditionsRaiseEvent",

  // procedure main
  ProcedureMainInitiateAndConfirmStep: "ProcedureMainInitiateAndConfirmStep",
  ProcedureMainInitiateAndConfirmStepDefinition:
    "ProcedureMainInitiateAndConfirmDefinition",
  ProcedureMainInitiateAndConfirmStepContinuationTest:
    "ProcedureMainInitiateAndConfirmStepContinuationTest",
  ProcedureMainInitiateAndConfirmActivity:
    "ProcedureMainInitiateAndConfirmActivity",
  ProcedureMainInitiateAndConfirmActivityConfirmed:
    "ProcedureMainInitiateAndConfirmActivityConfirmed",
  ProcedureMainInitiateAndConfirmActivityNotConfirmed:
    "ProcedureMainInitiateAndConfirmActivityNotConfirmed",
  ProcedureMainInitiateAndConfirmActivityAborted:
    "ProcedureMainInitiateAndConfirmActivityAborted",
  ProcedureMainInitiateActivity: "ProcedureMainInitiateActivity",
  ProcedureMainInitiateActivityArgument:
    "ProcedureMainInitiateActivityArgument",
  ProcedureMainInitiateActivityDirective:
    "ProcedureMainInitiateActivityDirective",
  ProcedureMainInitiateActivityReferBy: "ProcedureMainInitiateActivityReferBy",
  ProcedureMainInformUser: "ProcedureMainInformUser",
  ProcedureMainLog: "ProcedureMainLog",

  // procedure watchdog
  ProcedureWatchdog: "ProcedureWatchdog",
  ProcedureWatchdogInitiateAndConfirmStep:
    "ProcedureWatchdogInitiateAndConfirmStep",
  ProcedureWatchdogInitiateAndConfirmStepDefinition:
    "ProcedureWatchdogInitiateAndConfirmStepDefinition",

  // procedure confirmation
  ProcedureConfirmationWaitUntilAbsoluteTime:
    "ProcedureConfirmationWaitUntilAbsoluteTime",
  ProcedureConfirmationWaitForRelativeTime:
    "ProcedureConfirmationWaitForRelativeTime",
  ProcedureConfirmationWaitUntilTrue: "ProcedureConfirmationWaitUntilTrue",
  ProcedureConfirmationIfConditionTrue: "ProcedureConfirmationIfConditionTrue",
  ProcedureConfirmationTimeout: "ProcedureConfirmationTimeout",
  ProcedureConfirmationExpression: "ProcedureConfirmationExpression",
  ProcedureConfirmationExpressionValue: "ProcedureConfirmationExpressionValue",
  ProcedureConfirmationTimeInterval: "ProcedureConfirmationTimeInterval",
  ProcedureConfirmationRaiseEvent: "ProcedureConfirmationRaiseEvent",

  // step declaration
  StepDeclarationDeclareEvent: "StepDeclarationDeclareEvent",
  StepDeclarationDescribedBy: "StepDeclarationDescribedBy",
  StepDeclarationDeclareVariable: "StepDeclarationDeclareVariable",

  // step preconditions
  StepPreconditionsWaitUntilAbsoluteTime:
    "StepPreconditionsWaitUntilAbsoluteTime",
  StepPreconditionsWaitForRelativeTime: "StepPreconditionsWaitForRelativeTime",
  StepPreconditionsWaitUntilTrue: "StepPreconditionsWaitUntilTrue",
  StepPreconditionsIfConditionTrue: "StepPreconditionsIfConditionTrue",
  StepPreconditionsTimeout: "StepPreconditionsTimeout",
  StepPreconditionsExpression: "StepPreconditionsExpression",
  StepPreconditionsExpressionValue: "ProcedurePreconditionsExpressionValue",
  StepPreconditionsTimeInterval: "StepPreconditionsTimeInterval",
  StepPreconditionsRaiseEvent: "StepPreconditionsRaiseEvent",

  // step main body
  StepMainSetContext: "StepMainSetContext",
  StepMainAssignmentStatement: "StepMainAssignmentStatement",
  StepMainLogic: "StepMainLogic",
  StepMainExpression: "StepMainExpression",
  StepMainWaitUntilAbsoluteTime: "StepMainWaitUntilAbsoluteTime",
  StepMainWaitForRelativeTime: "StepMainWaitForRelativeTime",
  StepMainWaitForEvent: "StepMainWaitForEvent",
  StepMainWaitUntilTrue: "StepMainWaitUntilTrue",
  StepMainExpressionValue: "StepMainExpressionValue",
  StepMainTimeInterval: "StepMainTimeInterval",
  StepMainWaitTimeout: "StepMainWaitTimeout",
  StepMainRaiseEvent: "StepMainRaiseEvent",
  StepMainInitiateAndConfirmStep: "StepMainInitiateAndConfirmStep",
  StepMainInitiateAndConfirmActivity: "StepMainInitiateAndConfirmActivity",
  StepMainInitiateActivity: "StepMainInitiateActivity",
  StepMainLog: "StepMainLog",
  StepMainInformUser: "StepMainInformUser",

  // step watchdog
  StepWatchdogInitiateAndConfirmStep: "StepWatchdogInitiateAndConfirmStep",

  // step confirmation
  // procedure confirmation
  StepConfirmationWaitUntilAbsoluteTime:
    "StepConfirmationWaitUntilAbsoluteTime",
  StepConfirmationWaitForRelativeTime:
    "StepConfirmationWaitForRelativeTime",
  StepConfirmationWaitUntilTrue: "StepConfirmationWaitUntilTrue",
  StepConfirmationIfConditionTrue: "StepConfirmationIfConditionTrue",
  StepConfirmationTimeout: "StepConfirmationTimeout",
  StepConfirmationExpression: "StepConfirmationExpression",
  StepConfirmationExpressionValue: "StepConfirmationExpressionValue",
  StepConfirmationTimeInterval: "StepConfirmationTimeInterval",
  StepConfirmationRaiseEvent: "StepConfirmationRaiseEvent",

  // ProcedureMainInitiateAndConfirmStepDefinitionPlaceholder:
  //   "ProcedureMainInitiateAndConfirmStepDefinitionPlaceholder",
};
