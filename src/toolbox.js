import { Colors } from "./colors";

export const toolbox = {
  kind: "categoryToolbox",
  contents: [
    {
      kind: "category",
      name: "Procedure declaration",
      colour: Colors.CategoryProcedure,
      contents: [
        {
          kind: "block",
          type: "procedure_declaration_declare_event",
          inputs: {
            extra: {
              shadow: {
                type: "described_by_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "procedure_declaration_described_by",
        },
      ],
    },
    {
      kind: "category",
      name: "Procedure preconditions",
      colour: Colors.CategoryProcedure,
      contents: [
        {
          kind: "block",
          type: "procedure_preconditions_wait_until_absolute_time",
        },
        {
          kind: "block",
          type: "procedure_preconditions_wait_for_relative_time",
        },
        {
          kind: "block",
          type: "procedure_preconditions_wait_until_true",
          inputs: {
            expression: {
              shadow: {
                type: "expression_placeholder",
              },
            },
            extra: {
              shadow: {
                type: "timeout_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "procedure_preconditions_if_condition_true",
          inputs: {
            value: {
              shadow: {
                type: "expression_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "procedure_preconditions_relational_expression",
        },
        {
          kind: "block",
          type: "procedure_preconditions_timeout",
          inputs: {
            event: {
              shadow: {
                type: "raise_event_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "procedure_preconditions_raise_event",
        },
      ],
    },
    {
      kind: "category",
      name: "Procedure main",
      colour: Colors.CategoryProcedure,
      contents: [
        {
          kind: "block",
          type: "procedure_main_initiate_and_confirm_step",
          inputs: {
            step_definition: {
              shadow: {
                type: "step_definition_placeholder",
              },
            },
            continuation_test: {
              shadow: {
                type: "continuation_test_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "procedure_main_initiate_and_confirm_activity",
          inputs: {
            refer_by: {
              shadow: {
                type: "refer_by_placeholder",
              },
            },
            continuation_test: {
              shadow: {
                type: "continuation_test_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "procedure_main_initiate_activity",
          inputs: {
            refer_by: {
              shadow: {
                type: "refer_by_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "procedure_main_initiate_and_confirm_step_definition",
        },
        {
          kind: "block",
          type: "procedure_main_initiate_and_confirm_step_continuation_test",
        },
        {
          kind: "block",
          type: "procedure_main_initiate_activity_refer_by",
        },
        {
          kind: "block",
          type: "procedure_main_inform_user",
        },
        {
          kind: "block",
          type: "procedure_main_log",
        },
      ],
    },
    {
      kind: "category",
      name: "Procedure watchdog",
      colour: Colors.CategoryProcedure,
      contents: [
        {
          kind: "block",
          type: "procedure_watchdog_initiate_and_confirm_step",
          inputs: {
            step_definition: {
              shadow: {
                type: "step_definition_placeholder",
              },
            },
            continuation_test: {
              shadow: {
                type: "continuation_test_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "procedure_watchdog_initiate_and_confirm_step_definition",
        },
        {
          kind: "block",
          type: "procedure_watchdog_initiate_and_confirm_step_continuation_test",
        },
      ],
    },
    {
      kind: "category",
      name: "Procedure confirmation",
      colour: Colors.CategoryProcedure,
      contents: [
        {
          kind: "block",
          type: "procedure_confirmation_wait_until_absolute_time",
        },
        {
          kind: "block",
          type: "procedure_confirmation_wait_for_relative_time",
        },
        {
          kind: "block",
          type: "procedure_confirmation_wait_until_true",
          inputs: {
            expression: {
              shadow: {
                type: "expression_placeholder",
              },
            },
            extra: {
              shadow: {
                type: "timeout_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "procedure_confirmation_if_condition_true",
          inputs: {
            value: {
              shadow: {
                type: "expression_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "procedure_confirmation_relational_expression",
        },
        {
          kind: "block",
          type: "procedure_confirmation_timeout",
          inputs: {
            event: {
              shadow: {
                type: "raise_event_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "procedure_confirmation_raise_event",
        },
      ],
    },
    {
      kind: "category",
      name: "Step declaration",
      colour: Colors.CategoryStep,
      contents: [
        {
          kind: "block",
          type: "step_declaration_declare_variable",
          inputs: {
            extra: {
              shadow: {
                type: "described_by_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "step_declaration_declare_event",
          inputs: {
            extra: {
              shadow: {
                type: "described_by_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "step_declaration_described_by",
        },
      ],
    },
    {
      kind: "category",
      name: "Step preconditions",
      colour: Colors.CategoryStep,
      contents: [
        {
          kind: "block",
          type: "step_preconditions_wait_until_absolute_time",
        },
        {
          kind: "block",
          type: "step_preconditions_wait_for_relative_time",
        },
        {
          kind: "block",
          type: "step_preconditions_wait_until_true",
          inputs: {
            expression: {
              shadow: {
                type: "expression_placeholder",
              },
            },
            extra: {
              shadow: {
                type: "timeout_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "step_preconditions_if_condition_true",
          inputs: {
            value: {
              shadow: {
                type: "expression_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "step_preconditions_relational_expression",
        },
        {
          kind: "block",
          type: "step_preconditions_timeout",
          inputs: {
            event: {
              shadow: {
                type: "raise_event_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "step_preconditions_raise_event",
        },
      ],
    },
    {
      kind: "category",
      name: "Step main",
      colour: Colors.CategoryStep,
      contents: [
        {
          kind: "block",
          type: "step_main_set_context",
        },
        {
          kind: "block",
          type: "step_main_assignment_statement",
        },
        {
          kind: "block",
          type: "step_main_logic_if",
          inputs: {
            expression: {
              shadow: {
                type: "expression_placeholder",
              },
            },
          },
        },

        {
          kind: "block",
          type: "step_main_logic_repeat_statement",
          inputs: {
            expression: {
              shadow: {
                type: "expression_placeholder",
              },
            },
            timeout: {
              shadow: {
                type: "timeout_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "step_main_logic_case",
        },
        {
          kind: "block",
          type: "step_main_logic_while",
          inputs: {
            expression: {
              shadow: {
                type: "expression_placeholder",
              },
            },
            timeout: {
              shadow: {
                type: "timeout_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type:"step_main_logic_for_statement",
        },
        {
          kind: "block",
          type: "step_main_wait_until_absolute_time",
        },
        {
          kind: "block",
          type: "step_main_wait_for_relative_time",
        },
        {
          kind: "block",
          type: "step_main_wait_for_event",
        },
        {
          kind: "block",
          type: "step_main_wait_until_true",
          inputs: {
            expression: {
              shadow: {
                type: "expression_placeholder",
              },
            },
            extra: {
              shadow: {
                type: "timeout_placeholder",
              },
            },
          },
        },

        {
          kind: "block",
          type: "step_main_initiate_and_confirm_step",
          inputs: {
            step_definition: {
              shadow: {
                type: "step_definition_placeholder",
              },
            },
            continuation_test: {
              shadow: {
                type: "continuation_test_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "step_main_initiate_and_confirm_activity",
          inputs: {
            refer_by: {
              shadow: {
                type: "refer_by_placeholder",
              },
            },
            continuation_test: {
              shadow: {
                type: "continuation_test_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "step_main_initiate_activity",
          inputs: {
            refer_by: {
              shadow: {
                type: "refer_by_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "step_main_inform_user",
        },
        {
          kind: "block",
          type: "step_main_log",
        },

        {
          kind: "block",
          type: "step_main_logic_expression",
        },

        {
          kind: "block",
          type: "step_main_wait_timeout",
          inputs: {
            event: {
              shadow: {
                type: "raise_event_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "step_main_raise_event",
        },

        {
          kind: "block",
          type: "procedure_main_initiate_and_confirm_step_definition",
        },
        {
          kind: "block",
          type: "procedure_main_initiate_and_confirm_step_continuation_test",
        },
        {
          kind: "block",
          type: "procedure_main_initiate_activity_refer_by",
        },
      ],
    },
    {
      kind: "category",
      name: "Step watchdog",
      colour: Colors.CategoryStep,
      contents: [
        {
          kind: "block",
          type: "step_watchdog_initiate_and_confirm_step",
          inputs: {
            step_definition: {
              shadow: {
                type: "step_definition_placeholder",
              },
            },
            continuation_test: {
              shadow: {
                type: "continuation_test_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "procedure_watchdog_initiate_and_confirm_step_definition",
        },
        {
          kind: "block",
          type: "procedure_watchdog_initiate_and_confirm_step_continuation_test",
        },
      ],
    },
    {
      kind: "category",
      name: "Step confirmation",
      colour: Colors.CategoryStep,
      contents: [
        {
          kind: "block",
          type: "step_confirmation_wait_until_absolute_time",
        },
        {
          kind: "block",
          type: "step_confirmation_wait_for_relative_time",
        },
        {
          kind: "block",
          type: "step_confirmation_wait_until_true",
          inputs: {
            expression: {
              shadow: {
                type: "expression_placeholder",
              },
            },
            extra: {
              shadow: {
                type: "timeout_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "step_confirmation_if_condition_true",
          inputs: {
            value: {
              shadow: {
                type: "expression_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "step_confirmation_relational_expression",
        },
        {
          kind: "block",
          type: "step_confirmation_timeout",
          inputs: {
            event: {
              shadow: {
                type: "raise_event_placeholder",
              },
            },
          },
        },
        {
          kind: "block",
          type: "step_confirmation_raise_event",
        },
      ],
    },
  ],
};
