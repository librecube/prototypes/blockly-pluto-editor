import * as Blockly from "blockly";
import { Colors } from "../../colors";
import { Types } from "../types";

export const all_step_confirmation_types = [
  Types.StepConfirmationWaitUntilAbsoluteTime,
  Types.StepConfirmationWaitForRelativeTime,
  Types.StepConfirmationWaitUntilTrue,
  Types.StepConfirmationIfConditionTrue,
];

export const step_confirmation_wait_until_absolute_time = {
  type: "step_confirmation_wait_until_absolute_time",
  message0: "wait until absolute time %1",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "yyyy-mm-ddThh:mm:ss.sssZ",
    },
  ],
  previousStatement: Types.StepConfirmationWaitUntilAbsoluteTime,
  nextStatement: all_step_confirmation_types,
  colour: Colors.BlockConfirmation,
  tooltip: "Wait until a given absolute time.",
};

export const step_confirmation_wait_for_relative_time = {
  type: "step_confirmation_wait_for_relative_time",
  message0: "wait for relative time %1",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "relative_time",
    },
  ],
  previousStatement: Types.StepConfirmationWaitForRelativeTime,
  nextStatement: all_step_confirmation_types,
  colour: Colors.BlockConfirmation,
  tooltip: "Wait for a given time to elapse.",
};

export const step_confirmation_wait_until_true = {
  type: "step_confirmation_wait_until_true",
  message0: "wait until true %1 %2",
  args0: [
    {
      type: "input_value",
      name: "expression",
      check: [Types.StepConfirmationExpression],
    },
    {
      type: "input_value",
      name: "extra",
      check: [Types.StepConfirmationTimeout],
    },
  ],
  inputsInline: true,
  previousStatement: Types.StepConfirmationWaitUntilTrue,
  nextStatement: all_step_confirmation_types,
  colour: Colors.BlockConfirmation,
  tooltip: "Wait until a given conditions become true.",
};

export const step_confirmation_if_condition_true = {
  type: "step_confirmation_if_condition_true",
  message0: "if %1",
  args0: [
    {
      type: "input_value",
      name: "value",
      check: [Types.StepConfirmationExpression],
    },
  ],
  inputsInline: true,
  previousStatement: Types.StepConfirmationIfConditionTrue,
  nextStatement: all_step_confirmation_types,
  colour: Colors.BlockConfirmation,
  tooltip: "If the result of a logical expression is true.",
};

export const step_confirmation_timeout = {
  type: "step_confirmation_timeout",
  message0: "timeout %1 %2",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "relative_time",
    },
    {
      type: "input_value",
      name: "event",
      check: [Types.StepConfirmationRaiseEvent],
    },
  ],
  inputsInline: true,
  output: Types.StepConfirmationTimeout,
  colour: Colors.BlockConfirmation,
  tooltip: "Defines the timeout for waiting.",
};

export const step_confirmation_raise_event = {
  type: "step_confirmation_raise_event",
  message0: "raise event %1",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "event_name",
    },
  ],
  output: Types.StepConfirmationRaiseEvent,
  colour: Colors.BlockConfirmation,
  tooltip: "The event to raise when timeout happened.",
};

export const step_confirmation_relational_expression = {
  type: "step_confirmation_relational_expression",
  message0: "expression %1 %2 %3",
  args0: [
    {
      type: "field_input",
      name: "input1",
      text: "value1",
    },
    {
      type: "field_dropdown",
      name: "values",
      options: [
        ["=", "="],
        ["!=", "!="],
        ["<", "<"],
        [">", ">"],
        ["<=", "<="],
        [">=", ">="],
      ],
    },
    {
      type: "field_input",
      name: "input2",
      text: "value2",
    },
  ],
  output: Types.StepConfirmationExpression,
  colour: Colors.BlockConfirmation,
  tooltip: "A logic expression that evaluates to true or false.",
};
