import * as Blockly from "blockly";
import { Colors } from "../../colors";
import { Types } from "../types";

export const all_procedure_declaration_types = [
  Types.ProcedureDeclarationDeclareEvent,
];

export const procedure_declaration_declare_event = {
  type: "procedure_declaration_declare_event",
  message0: "event %1 %2",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "event_name",
    },
    {
      type: "input_value",
      name: "extra",
      check: [Types.ProcedureDeclarationDescribedBy],
    },
  ],
  inputsInline: true,
  previousStatement: Types.ProcedureDeclarationDeclareEvent,
  nextStatement: all_procedure_declaration_types,
  colour: Colors.BlockDeclaration,
  tooltip: "Declares an event that can be raised within the procedure.",
};

export const procedure_declaration_described_by = {
  type: "procedure_declaration_described_by",
  message0: "described by %1",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "text",
    },
  ],
  output: Types.ProcedureDeclarationDescribedBy,
  colour: Colors.BlockDeclaration,
};
