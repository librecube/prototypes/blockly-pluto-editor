# Blockly Sample App

## Purpose

Blockly is an open-source software designed to utilize graphical blocks as code representations, enabling users to construct programs visually. The primary objective of this project is to develop an application with a visually appealing theme built upon Blockly that generates PLUTO scripts. Specifically, users can assemble blocks containing commands (e.g., to do the movement of our prototype rover) within a control logic framework. Subsequently, a corresponding PLUTO script will be automatically generated. Additionally, an optional feature of the project involves the creation of visualizations of the generated script in PDF format, thereby enhancing the project's functionality.

---
## Local development setup

> basic requirements


1. Install [nodejs](https://nodejs.org/en/download/current)
2. [Install](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) npm if you haven't before.
3. clone the following repository and cd into the repository.
4. Run `npm install` to install the required dependencies.
5. Run `npm run start` to run the development server and see the app in action.
6. If you make any changes to the source code, just refresh the browser while the server is running to see them.

> or you can use docker to run the project

`docker build -t <image_name> .`

`docker run -p 8080:8080 <image_name>`

---
## Important

> currently all custom created blocks are in `procedure category` and only `log` block is in `text category`.


> example script
![example](https://gitlab.com/librecube1/blockly-to-pluto/-/raw/main/examples/example.png)
---
## Structure

- `package.json` contains basic information about the app. This is where the scripts to run, build, etc. are listed.
- `package-lock.json` is used by npm to manage dependencies
- `webpack.config.js` is the configuration for webpack. This handles bundling the application and running our development server.
- `src/` contains the rest of the source code.
- `dist/` contains the packaged output (that you could host on a server, for example). This is ignored by git and will only appear after you run `npm run build` or `npm run start`.

---
### Source Code

- `index.html` contains the skeleton HTML for the page. This file is modified during the build to import the bundled source code output by webpack.
- `index.js` is the entry point of the app. It configures Blockly and sets up the page to show the blocks, the generated code, and the output of running the code in JavaScript.
- `serialization.js` has code to save and load the workspace using the browser's local storage. This is how your workspace is saved even after refreshing or leaving the page. You could replace this with code that saves the user's data to a cloud database instead.
- `toolbox.js` contains the toolbox definition for the all custom created pluto blocks.
- `blocks/pluto.js` has code for a custom pluto blocks.
- `generators/pluto.js` contains the pluto generator for the custom pluto blocks.

---
## Serving

To run your app locally, run `npm run start` to run the development server. This mode generates source maps and ingests the source maps created by Blockly, so that you can debug using unminified code.

To deploy your app so that others can use it, run `npm run build` to run a production build. This will bundle your code and minify it to reduce its size. You can then host the contents of the `dist` directory on a web server of your choosing. If you're just getting started, try using [GitHub Pages](https://pages.github.com/).
