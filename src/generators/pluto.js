import * as Blockly from "blockly";
export const plutoGenerator = new Blockly.Generator("PLUTO");

export const ORDER = {
  NONE: 0,
  ATOMIC: 99,
};

export const INDENT = "    ";

plutoGenerator.init = function (w) {
  plutoGenerator.nameDB_ = new Blockly.Names();
  plutoGenerator.INDENT = INDENT;
};

// this block generates all code for main block
plutoGenerator.scrub_ = function (block, code, thisOnly = false) {
  if (block.type != "procedure") {
    CheckValidBlock(block);
  }
  const nextBlock = block.nextConnection && block.nextConnection.targetBlock();
  if (nextBlock && !thisOnly) {
    return code + "\n" + plutoGenerator.blockToCode(nextBlock);
  }
  return code;
};

export function CheckValidBlock(block) {
  const parentBlock = block.getParent();
  const rootBlock = block.getRootBlock();
  if (parentBlock == null || rootBlock.type != "procedure") {
    block.setEnabled(false);
  } else {
    const children = block.getDescendants(true);
    children.forEach((child) => {
      child.setEnabled(true);
    });
  }
}

// procedure

plutoGenerator.forBlock["procedure"] = function (block, generator) {
  const declarations = generator.statementToCode(block, "declaration");
  const Preconditions = generator.statementToCode(block, "preconditions");
  const main = generator.statementToCode(block, "main");

  var watchdog = generator.statementToCode(block, "watchdog");
  const confirmations = generator.statementToCode(block, "confirmation");
  const declare_body = "  declare\n" + declarations + "\n  end declare\n";
  const precondition_body =
    "  preconditions\n" + Preconditions + "\n  end preconditions\n";
  const main_body = `  main\n${main}\n  end main`;
  const watchdog_body = "\n  watchdog\n" + watchdog + "\n  end watchdog";
  const confirmation_body =
    "\n  confirmation\n" + confirmations + "\n  end confirmation";
  const code = `procedure\n${declarations ? `${declare_body}` : ""}${Preconditions ? `${precondition_body}` : ""
    }${main_body}${watchdog ? `${watchdog_body}` : ""}${confirmations ? `${confirmation_body}` : ""
    }  \nend procedure`;
  return code;
};

// procedure declaration

plutoGenerator.forBlock["procedure_declaration_declare_event"] = function (
  block,
  generator
) {
  var value = block.getFieldValue("value");
  var value_extra = generator.valueToCode(block, "extra", ORDER.ATOMIC);
  var next_block = block.getNextBlock();
  var child = block.getInputTargetBlock("extra");

  var code = `event ${value}${child && child.type != "described_by_placeholder"
    ? `\n  ${value_extra}`
    : ""
    }${next_block ? "," : ""}`;
  return code;
};

plutoGenerator.forBlock["procedure_declaration_described_by"] = function (
  block,
  generator
) {
  const textValue = block.getFieldValue("value");
  const code = `described by "${textValue}"`;
  return [code, ORDER.NONE];
};

// procedure preconditions

plutoGenerator.forBlock["procedure_preconditions_wait_until_absolute_time"] =
  function (block, generator) {
    var absolute_value = block.getFieldValue("value");
    const expression_value = generator.valueToCode(
      block,
      "expression",
      ORDER.ATOMIC
    );
    var child = block.getNextBlock();
    var code = `wait until ${absolute_value}${child ? " then" : ""}`;
    return code;
  };

plutoGenerator.forBlock["procedure_preconditions_wait_for_relative_time"] =
  function (block, generator) {
    var text_value = block.getFieldValue("value");
    var expression = generator.valueToCode(block, "expression", ORDER.ATOMIC);
    var child = block.getNextBlock();
    var code = `wait for ${text_value}${child ? " then" : ""}`;
    return code;
  };

plutoGenerator.forBlock["procedure_preconditions_wait_until_true"] = function (
  block,
  generator
) {
  var value_expression = generator.valueToCode(
    block,
    "expression",
    ORDER.ATOMIC
  );
  var value_extra = generator.valueToCode(block, "extra", ORDER.ATOMIC);
  var next_block = block.getNextBlock();
  var child = block.getInputTargetBlock("extra");
  var code = `wait until ${value_expression}${child ? `\n  ${value_extra}` : ""
    }${next_block ? " then" : ""}`;
  return code;
};

plutoGenerator.forBlock["procedure_preconditions_if_condition_true"] =
  function (block, generator) {
    var value = generator.valueToCode(block, "value", ORDER.ATOMIC);
    var child = block.getNextBlock();
    var code = `if ${value}`;
    if (child) {
      code = `if ${value} then`;
    }
    return code;
  };

plutoGenerator.forBlock["procedure_preconditions_relational_expression"] =
  function (block, generator) {
    var value_input1 = block.getFieldValue("input1");
    var dropdown_values = block.getFieldValue("values");
    var value_input2 = block.getFieldValue("input2");
    var code = `${value_input1} ${dropdown_values} ${value_input2}`;
    return [code, 0];
  };

plutoGenerator.forBlock["procedure_preconditions_timeout"] = function (
  block,
  generator
) {
  var value_expression = generator.valueToCode(block, "event", ORDER.ATOMIC);
  var text_value = block.getFieldValue("value");
  var child = block.getInputTargetBlock("event");
  var code = `timeout ${text_value} ${child && child.type != "raise_event_placeholder"
    ? `\n  ${value_expression}`
    : ""
    }`;
  return [code, ORDER.NONE];
};

plutoGenerator.forBlock["procedure_preconditions_raise_event"] = function (
  block,
  generator
) {
  var text_value = block.getFieldValue("value");
  var code = `raise event ${text_value}`;
  return [code, ORDER.NONE];
};

// procedure main

plutoGenerator.forBlock["procedure_main_initiate_and_confirm_step"] = function (
  block,
  generator
) {
  var step_name = block.getFieldValue("step_name");
  var value_step_definition = generator.valueToCode(
    block,
    "step_definition",
    ORDER.ATOMIC
  );
  var value_continuation_test = generator.valueToCode(
    block,
    "continuation_test",
    ORDER.ATOMIC
  );
  var step_definition_child = block.getInputTargetBlock("step_definition");
  var continuation_test_child = block.getInputTargetBlock("continuation_test");

  var code = `initiate and confirm step ${step_name}${step_definition_child &&
    step_definition_child.type !=
    "step_definition_placeholder"
    ? `\n${value_step_definition}`
    : ""
    }${continuation_test_child &&
      continuation_test_child.type !=
      "continuation_test_placeholder"
      ? `${value_continuation_test}`
      : ""
    }${(step_definition_child &&
      step_definition_child.type !=
      "step_definition_placeholder") ||
      (continuation_test_child &&
        continuation_test_child.type !=
        "continuation_test_placeholder")
      ? `\nend step;`
      : ""
    }`;
  return code;
};

plutoGenerator.forBlock["procedure_main_initiate_and_confirm_step_definition"] =
  function (block, generator) {
    const declaration = generator.statementToCode(block, "declaration");
    const preconditions = generator.statementToCode(block, "preconditions");
    const main = generator.statementToCode(block, "main");
    var watchdog = generator.statementToCode(block, "watchdog");
    const confirmation = generator.statementToCode(block, "confirmation");
    const declare_body = "  declare\n" + declaration + "\n  end declare\n  ";
    const preconditions_body =
      "  preconditions\n" + preconditions + "\n  end preconditions\n  ";
    const main_body = `  main\n${main}\n  end main`;
    const watchdog_body = "\n  watchdog\n" + watchdog + "\n  end watchdog  ";
    const confirmation_body =
      "\n  confirmation\n" + confirmation + "\n  end confirmation  ";
    const code = `${declaration ? `${declare_body}` : ""}${preconditions ? `${preconditions_body}` : ""
      }${main_body}${watchdog ? `${watchdog_body}` : ""}${confirmation ? `${confirmation_body}` : ""
      }`;
    return [code, ORDER.NONE];
  };

plutoGenerator.forBlock[
  "procedure_main_initiate_and_confirm_step_continuation_test"
] = function (block, generator) {
  var continuationTestString = "";
  for (let i = 0; i < block.itemCountContinuationTest_; i++) {
    const ref = block.getFieldValue("CONTINUATION_NAME" + i);
    const val = block.getFieldValue("CONTINUATION_VALUE" + i);
    continuationTestString += `    ${ref} : ${val}${i == block.itemCountContinuationTest_ - 1 ? "" : ",\n"
      }`;
  }
  var code = `${continuationTestString != ""
    ? `\n  in case \n${continuationTestString}\n  end case`
    : ""
    }`;

  return [code, ORDER.NONE];
};

plutoGenerator.forBlock["procedure_main_initiate_and_confirm_activity"] =
  function (block, generator) {
    var activity_call = block.getFieldValue("activity_call");
    var value_refer_by = generator.valueToCode(block, "refer_by", ORDER.ATOMIC);
    var child = block.getInputTargetBlock("refer_by");
    var value_continuation_test = generator.valueToCode(
      block,
      "continuation_test",
      ORDER.ATOMIC
    );
    var continuation_test_child =
      block.getInputTargetBlock("continuation_test");
    var argumentsString = "";
    for (let i = 0; i < block.itemCount_; i++) {
      const ref = block.getFieldValue("ARG_NAME" + i);
      const val = block.getFieldValue("ARG_VALUE" + i);
      argumentsString += `    ${ref} := ${val}${i == block.itemCount_ - 1 ? "" : ",\n"
        }`;
    }
    var directiveString = "";
    for (let i = 0; i < block.itemCountDirective_; i++) {
      const ref = block.getFieldValue("DIRECTIVE_NAME" + i);
      const val = block.getFieldValue("DIRECTIVE_VALUE" + i);
      directiveString += `    ${ref} := ${val}${i == block.itemCountDirective_ - 1 ? "" : ",\n"
        }`;
    }

    var code = `initiate and confirm ${activity_call}${argumentsString != ""
      ? `\n  with arguments \n${argumentsString}\n  end with`
      : ""
      }${directiveString != ""
        ? `\n  with directives \n${directiveString}\n  end with`
        : ""
      }${child &&
        child.type !=
        "refer_by_placeholder"
        ? `\n  ${value_refer_by}`
        : ""
      }${continuation_test_child &&
        continuation_test_child.type !=
        "continuation_test_placeholder"
        ? `${value_continuation_test};`
        : ";"
      }`;
    return code;
  };

plutoGenerator.forBlock["procedure_main_initiate_activity"] = function (
  block,
  generator
) {
  var activity_call = block.getFieldValue("activity_call");
  var value_refer_by = generator.valueToCode(block, "refer_by", ORDER.ATOMIC);
  var child = block.getInputTargetBlock("refer_by");
  var argumentsString = "";
  for (let i = 0; i < block.itemCount_; i++) {
    const ref = block.getFieldValue("ARG_NAME" + i);
    const val = block.getFieldValue("ARG_VALUE" + i);
    argumentsString += `    ${ref} := ${val}${i == block.itemCount_ - 1 ? "" : ",\n"
      }`;
  }
  var directiveString = "";
  for (let i = 0; i < block.itemCountDirective_; i++) {
    const ref = block.getFieldValue("DIRECTIVE_NAME" + i);
    const val = block.getFieldValue("DIRECTIVE_VALUE" + i);
    directiveString += `    ${ref} := ${val}${i == block.itemCountDirective_ - 1 ? "" : ",\n"
      }`;
  }

  var code = `initiate ${activity_call}${argumentsString != ""
    ? `\n  with arguments \n${argumentsString}\n  end with`
    : ""
    }${directiveString != ""
      ? `\n  with directives \n${directiveString}\n  end with`
      : ""
    }${child &&
      child.type !=
      "refer_by_placeholder"
      ? `\n  ${value_refer_by};`
      : ";"
    }`;
  return code;
};

plutoGenerator.forBlock["procedure_main_initiate_activity_refer_by"] =
  function (block, generator) {
    var value = block.getFieldValue("value");
    var code = `refer by ${value}`;
    return [code, ORDER.NONE];
  };

plutoGenerator.forBlock["procedure_main_inform_user"] = function (
  block,
  generator
) {
  var text_value = block.getFieldValue("value");
  const code = `inform user "${text_value}";`;
  return code;
};

plutoGenerator.forBlock["procedure_main_log"] = function (block, generator) {
  const textValue = block.getFieldValue("value");
  var value_extra = generator.valueToCode(block, "extra", ORDER.ATOMIC);
  var child = block.getInputTargetBlock("extra");
  const code = `log "${child ? `${value_extra}` : `${textValue}`}";`;
  return code;
};

// procedure watchdog

plutoGenerator.forBlock["procedure_watchdog_initiate_and_confirm_step"] =
  function (block, generator) {
    var step_name = block.getFieldValue("step_name");
    var value_step_definition = generator.valueToCode(
      block,
      "step_definition",
      ORDER.ATOMIC
    );
    var value_continuation_test = generator.valueToCode(
      block,
      "continuation_test",
      ORDER.ATOMIC
    );
    var step_definition_child = block.getInputTargetBlock("step_definition");
    var continuation_test_child =
      block.getInputTargetBlock("continuation_test");

    var code = `initiate and confirm step ${step_name}${step_definition_child &&
      step_definition_child.type !=
      "step_definition_placeholder"
      ? `\n${value_step_definition}`
      : ""
      }${continuation_test_child &&
        continuation_test_child.type !=
        "continuation_test_placeholder"
        ? `${value_continuation_test}`
        : ""
      }${(step_definition_child &&
        step_definition_child.type !=
        "step_definition_placeholder") ||
        (continuation_test_child &&
          continuation_test_child.type !=
          "continuation_test_placeholder")
        ? `\nend step;`
        : ""
      }`;
    return code;
  };

plutoGenerator.forBlock[
  "procedure_watchdog_initiate_and_confirm_step_definition"
] = function (block, generator) {
  const declaration = generator.statementToCode(block, "declaration");
  const preconditions = generator.statementToCode(block, "preconditions");
  const main = generator.statementToCode(block, "main");
  var watchdog = generator.statementToCode(block, "watchdog");
  const confirmations = generator.statementToCode(block, "confirmation");
  const declare_body = "  declare\n" + declaration + "\n  end declare\n";
  const preconditions_body =
    "  preconditions\n" + preconditions + "\n  end preconditions\n";
  const main_body = `  main\n${main}\n  end main`;
  const watchdog_body = "\nwatchdog\n" + watchdog + "\n  end watchdog";
  const confirmation_body =
    "\n  confirmation\n" + confirmations + "\n  end confirmation";
  const code = `${declaration ? `${declare_body}` : ""}${preconditions ? `${preconditions_body}` : ""
    }${main_body}${watchdog ? `${watchdog_body}` : ""}${confirmations ? `${confirmation_body}` : ""
    }`;
  return [code, ORDER.NONE];
};

plutoGenerator.forBlock[
  "procedure_watchdog_initiate_and_confirm_step_continuation_test"
] = function (block, generator) {
  var continuationTestString = "";
  for (let i = 0; i < block.itemCountContinuationTest_; i++) {
    const ref = block.getFieldValue("CONTINUATION_NAME" + i);
    const val = block.getFieldValue("CONTINUATION_VALUE" + i);
    continuationTestString += `    ${ref} : ${val}${i == block.itemCountContinuationTest_ - 1 ? "" : ",\n"
      }`;
  }
  var code = `${continuationTestString != ""
    ? `\n  in case \n${continuationTestString}\n  end case`
    : ""
    }`;

  return [code, ORDER.NONE];
};

// procedure confirmation

plutoGenerator.forBlock["procedure_confirmation_wait_until_absolute_time"] =
  plutoGenerator.forBlock["procedure_preconditions_wait_until_absolute_time"];
plutoGenerator.forBlock["procedure_confirmation_wait_for_relative_time"] =
  plutoGenerator.forBlock["procedure_preconditions_wait_for_relative_time"];
plutoGenerator.forBlock["procedure_confirmation_wait_until_true"] =
  plutoGenerator.forBlock["procedure_preconditions_wait_until_true"];
plutoGenerator.forBlock["procedure_confirmation_if_condition_true"] =
  plutoGenerator.forBlock["procedure_preconditions_if_condition_true"];
plutoGenerator.forBlock["procedure_confirmation_relational_expression"] =
  plutoGenerator.forBlock["procedure_preconditions_relational_expression"];
plutoGenerator.forBlock["procedure_confirmation_timeout"] =
  plutoGenerator.forBlock["procedure_preconditions_timeout"];
plutoGenerator.forBlock["procedure_confirmation_raise_event"] =
  plutoGenerator.forBlock["procedure_preconditions_raise_event"];

// step

// step declaration

plutoGenerator.forBlock["step_declaration_declare_variable"] = function (
  block,
  generator
) {
  var value = block.getFieldValue("value");
  var dropdown_types = block.getFieldValue("types");
  var value_extra = generator.valueToCode(block, "extra", ORDER.ATOMIC);
  var next_block = block.getNextBlock();
  var child = block.getInputTargetBlock("extra");

  var code = `variable ${value} of type ${dropdown_types}${child && child.type != "described_by_placeholder"
    ? `\n  ${value_extra}`
    : ""
    }${next_block ? "," : ""}`;

  return code;
};

plutoGenerator.forBlock["step_declaration_declare_event"] = function (
  block,
  generator
) {
  var value = block.getFieldValue("value");
  var value_extra = generator.valueToCode(block, "extra", ORDER.ATOMIC);
  var next_block = block.getNextBlock();
  var child = block.getInputTargetBlock("extra");

  var code = `event ${value}${child && child.type != "described_by_placeholder"
    ? `\n  ${value_extra}`
    : ""
    }${next_block ? "," : ""}`;
  return code;
};

plutoGenerator.forBlock["step_declaration_described_by"] = function (
  block,
  generator
) {
  var text_value = block.getFieldValue("value");
  var code = `described by "${text_value}"`;
  return [code, ORDER.NONE];
};

// step preconditions

plutoGenerator.forBlock["step_preconditions_wait_until_absolute_time"] =
  plutoGenerator.forBlock["procedure_preconditions_wait_until_absolute_time"];

plutoGenerator.forBlock["step_preconditions_wait_for_relative_time"] =
  plutoGenerator.forBlock["procedure_preconditions_wait_for_relative_time"];

plutoGenerator.forBlock["step_preconditions_wait_until_true"] =
  plutoGenerator.forBlock["procedure_preconditions_wait_until_true"];

plutoGenerator.forBlock["step_preconditions_if_condition_true"] =
  plutoGenerator.forBlock["procedure_preconditions_if_condition_true"];

plutoGenerator.forBlock["step_preconditions_timeout"] =
  plutoGenerator.forBlock["procedure_preconditions_timeout"];

plutoGenerator.forBlock["step_preconditions_raise_event"] =
  plutoGenerator.forBlock["procedure_preconditions_raise_event"];

plutoGenerator.forBlock["step_preconditions_relational_expression"] =
  plutoGenerator.forBlock["procedure_preconditions_relational_expression"];

// step main

plutoGenerator.forBlock["step_main_set_context"] = function (block, generator) {
  const statementMembers = generator.statementToCode(block, "main");
  const textValue = block.getFieldValue("value");
  const code =
    `in the context of ${textValue} do \n` +
    statementMembers +
    `\nend context;`;
  return code;
};

plutoGenerator.forBlock["step_main_assignment_statement"] = function (
  block,
  generator
) {
  var text_name = block.getFieldValue("name");
  var text_value = block.getFieldValue("value");

  var code = `${text_name} := ${text_value}`;
  return code;
};

plutoGenerator.forBlock["step_main_logic_if"] = function (block, generator) {
  var value_expression = generator.valueToCode(
    block,
    "expression",
    ORDER.ATOMIC
  );
  var statements = generator.statementToCode(block, "statement");
  var else_statements = generator.statementToCode(block, "else_statements");
  var code = `if ${value_expression} then\n${statements}${else_statements ? `\nelse \n${else_statements}` : ""
    }\nend if;`;
  return code;
};

plutoGenerator.forBlock["step_main_logic_expression"] = function (
  block,
  generator
) {
  var value_input1 = block.getFieldValue("input1");
  var dropdown_values = block.getFieldValue("values");
  var value_input2 = block.getFieldValue("input2");
  var code = `${value_input1} ${dropdown_values} ${value_input2}`;
  return [code, 0];
};

plutoGenerator.forBlock["step_main_logic_repeat_statement"] = function (
  block,
  generator
) {
  var statements = generator.statementToCode(block, "statements");
  var value_expression = generator.valueToCode(
    block,
    "expression",
    ORDER.ATOMIC
  );
  var value_timeout = generator.valueToCode(block, "timeout", ORDER.ATOMIC);
  var child = block.getInputTargetBlock("timeout");
  var code = `repeat \n${statements}\nuntil ${value_expression}${child && child.type != "timeout_placeholder"
    ? `\n ${value_timeout}`
    : ""
    }`;
  return code;
};

plutoGenerator.forBlock["step_main_logic_case"] = function (block, generator) {
  var expression = block.getFieldValue("expression");
  var case_tag = block.getFieldValue("case_tag");
  var statements = generator.statementToCode(block, "statements");
  var otherwise_statements = generator.statementToCode(
    block,
    "otherwise_statements"
  );
  var orIsString = "";
  for (let i = 0; i < block.itemCount_; i++) {
    const val = block.getFieldValue("OR_IS_VALUE" + i);
    orIsString += `  or is ${val}:\n${generator.statementToCode(
      block,
      "OR_IS" + i
    )}\n`;
  }

  var code = `in case ${expression}\n  is ${case_tag}:\n${statements}\n${
    orIsString != "" ? `${orIsString}` : ""
  }${
    otherwise_statements ? `  otherwise: \n${otherwise_statements}\n` : ""
  }end case;`;
  return code;
};

plutoGenerator.forBlock["step_main_logic_while"] = function (
  block,
  generator
) {
  var statements = generator.statementToCode(block, "statements");
  var value_expression = generator.valueToCode(
    block,
    "expression",
    ORDER.ATOMIC
  );
  var value_timeout = generator.valueToCode(block, "timeout", ORDER.ATOMIC);
  var child = block.getInputTargetBlock("timeout");
  var code = `while ${value_expression} ${
    child && child.type != "timeout_placeholder" ? `\n ${value_timeout}\n` : ""
  }do\n${statements}\nend while`;
  return code;
};

plutoGenerator.forBlock["step_main_logic_for_statement"] = function (block, generator) {
  const variable_name = block.getFieldValue("variable_name");
  const expression = block.getFieldValue("expression");
  const expression2 = block.getFieldValue("expression2");
  const increment = block.getFieldValue("increment");
  const statements = generator.statementToCode(block, "statements");

  const code = `for ${variable_name} := ${expression} to ${expression2} ${
    increment != "1" ? `by ${increment}` : ""
  }\ndo\n${statements}\nend for`;
  return code;
};

plutoGenerator.forBlock["step_main_wait_until_absolute_time"] = function (
  block,
  generator
) {
  var absolute_value = block.getFieldValue("value");
  var code = `wait until ${absolute_value};`;
  return code;
};

plutoGenerator.forBlock["step_main_wait_for_relative_time"] = function (
  block,
  generator
) {
  var text_value = block.getFieldValue("value");
  var code = `wait for ${text_value};`;
  return code;
};

plutoGenerator.forBlock["step_main_wait_for_event"] = function (
  block,
  generator
) {
  var text_value = block.getFieldValue("value");
  var code = `wait for ${text_value};`;
  return code;
};

plutoGenerator.forBlock["step_main_wait_until_true"] = function (
  block,
  generator
) {
  var value_expression = generator.valueToCode(
    block,
    "expression",
    ORDER.ATOMIC
  );
  var value_extra = generator.valueToCode(block, "extra", ORDER.ATOMIC);
  var child = block.getInputTargetBlock("extra");
  var code = `wait until ${value_expression}${child && child.type != "timeout_placeholder"
    ? `\n  ${value_extra}`
    : ""
    };`;
  return code;
};

plutoGenerator.forBlock["step_main_wait_timeout"] =
  plutoGenerator.forBlock["procedure_preconditions_timeout"];

plutoGenerator.forBlock["step_main_raise_event"] =
  plutoGenerator.forBlock["procedure_preconditions_raise_event"];

plutoGenerator.forBlock["step_main_initiate_and_confirm_step"] =
  plutoGenerator.forBlock["procedure_main_initiate_and_confirm_step"];

plutoGenerator.forBlock["step_main_initiate_and_confirm_activity"] =
  plutoGenerator.forBlock["procedure_main_initiate_and_confirm_activity"];

plutoGenerator.forBlock["step_main_initiate_activity"] =
  plutoGenerator.forBlock["procedure_main_initiate_activity"];

plutoGenerator.forBlock["step_main_inform_user"] =
  plutoGenerator.forBlock["procedure_main_inform_user"];

plutoGenerator.forBlock["step_main_log"] =
  plutoGenerator.forBlock["procedure_main_log"];

// step watchdog
plutoGenerator.forBlock["step_watchdog_initiate_and_confirm_step"] =
  plutoGenerator.forBlock["procedure_watchdog_initiate_and_confirm_step"];

  plutoGenerator.forBlock["step_confirmation_wait_until_absolute_time"] =
    plutoGenerator.forBlock["procedure_preconditions_wait_until_absolute_time"];
  plutoGenerator.forBlock["step_confirmation_wait_for_relative_time"] =
    plutoGenerator.forBlock["procedure_preconditions_wait_for_relative_time"];
  plutoGenerator.forBlock["step_confirmation_wait_until_true"] =
    plutoGenerator.forBlock["procedure_preconditions_wait_until_true"];
  plutoGenerator.forBlock["step_confirmation_if_condition_true"] =
    plutoGenerator.forBlock["procedure_preconditions_if_condition_true"];
  plutoGenerator.forBlock["step_confirmation_relational_expression"] =
    plutoGenerator.forBlock["step_preconditions_relational_expression"];
  plutoGenerator.forBlock["step_confirmation_timeout"] =
    plutoGenerator.forBlock["procedure_preconditions_timeout"];
  plutoGenerator.forBlock["step_confirmation_raise_event"] =
    plutoGenerator.forBlock["procedure_preconditions_raise_event"];


// placeholders

const placeholder = function (block, generator) {
  return ["", ORDER.NONE];
};

plutoGenerator.forBlock["step_definition_placeholder"] = placeholder;

plutoGenerator.forBlock["continuation_test_placeholder"] = placeholder;

plutoGenerator.forBlock["refer_by_placeholder"] = placeholder;

plutoGenerator.forBlock["described_by_placeholder"] = placeholder;

plutoGenerator.forBlock["expression_placeholder"] = placeholder;

plutoGenerator.forBlock["timeout_placeholder"] = placeholder;

plutoGenerator.forBlock["raise_event_placeholder"] = placeholder;
