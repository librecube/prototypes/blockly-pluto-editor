import * as Blockly from "blockly";
import { Colors } from "../../colors";
import { Types } from "../types";

export const all_step_declaration_types = [
  Types.StepDeclarationDeclareVariable,
  Types.StepDeclarationDeclareEvent,
  Types.StepDeclarationEnumeratedSet,
];


export const step_declaration_declare_variable = {
  type: "step_declaration_declare_variable",
  message0: "variable %1 of type %2 %3",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "variable_name",
    },
    {
      type: "field_dropdown",
      name: "types",
      options: [
        ["Boolean", "Boolean"],
        ["signed integer", "signed integer"],
        ["unsigned integer", "unsigned integer"],
        ["real", "real"],
        ["string", "string"],
        ["absolute time", "absolute time"],
        ["relative time", "relative time"],
      ],
    },
    {
      type: "input_value",
      name: "extra",
      check: [Types.StepDeclarationDescribedBy],
    },
  ],
  inputsInline: true,
  previousStatement: Types.StepDeclarationDeclareVariable,
  nextStatement: all_step_declaration_types,
  colour: Colors.BlockDeclaration,
  tooltip: "Declares a local variable that is used within the step.",
};

export const step_declaration_declare_event = {
  type: "step_declaration_declare_event",
  message0: "event %1 %2",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "event_name",
    },
    {
      type: "input_value",
      name: "extra",
      check: [Types.StepDeclarationDescribedBy],
    },
  ],
  inputsInline: true,
  previousStatement: Types.StepDeclarationDeclareEvent,
  nextStatement: all_step_declaration_types,
  colour: Colors.BlockDeclaration,
  tooltip: "Declares a local event that can be raised within the step.",
};

export const step_declaration_described_by = {
  type: "step_declaration_described_by",
  message0: "described by %1",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "text",
    },
  ],
  output: Types.StepDeclarationDescribedBy,
  colour: Colors.BlockDeclaration,
};
