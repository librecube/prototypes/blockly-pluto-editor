import * as Blockly from "blockly";
import { Colors } from "../../colors";
import { Types } from "../types";

export const all_step_preconditions_types = [
  Types.StepPreconditionsWaitUntilAbsoluteTime,
  Types.StepPreconditionsWaitForRelativeTime,
  Types.StepPreconditionsWaitUntilTrue,
  Types.StepPreconditionsIfConditionTrue,
];

export const step_preconditions_wait_until_absolute_time = {
  type: "step_preconditions_wait_until_absolute_time",
  message0: "wait until absolute time %1",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "yyyy-mm-ddThh:mm:ss.sssZ",
    },
  ],
  previousStatement: Types.StepPreconditionsWaitUntilAbsoluteTime,
  nextStatement: all_step_preconditions_types,
  colour: Colors.BlockPreconditions,
  tooltip: "Wait until a given absolute time.",
};

export const step_preconditions_wait_for_relative_time = {
  type: "step_preconditions_wait_for_relative_time",
  message0: "wait for relative time %1",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "relative_time",
    },
  ],
  previousStatement: Types.StepPreconditionsWaitForRelativeTime,
  nextStatement: all_step_preconditions_types,
  colour: Colors.BlockPreconditions,
  tooltip: "Wait for a given time to elapse.",
};

export const step_preconditions_wait_until_true = {
  type: "step_preconditions_wait_until_true",
  message0: "wait until true %1 %2",
  args0: [
    {
      type: "input_value",
      name: "expression",
      check: [Types.StepPreconditionsExpression],
    },
    {
      type: "input_value",
      name: "extra",
      check: [Types.StepPreconditionsTimeout],
    },
  ],
  inputsInline: true,
  previousStatement: Types.StepPreconditionsWaitUntilTrue,
  nextStatement: all_step_preconditions_types,
  colour: Colors.BlockPreconditions,
  tooltip: "Wait until a given conditions become true.",
};

export const step_preconditions_if_condition_true = {
  type: "step_preconditions_if_condition_true",
  message0: "if %1",
  args0: [
    {
      type: "input_value",
      name: "value",
      check: [Types.StepPreconditionsExpression],
    },
  ],
  inputsInline: true,
  previousStatement: Types.StepPreconditionsIfConditionTrue,
  nextStatement: all_step_preconditions_types,
  colour: Colors.BlockPreconditions,
  tooltip: "If the result of a logical expression is true.",
};

export const step_preconditions_timeout = {
  type: "step_preconditions_timeout",
  message0: "timeout %1 %2",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "relative_time",
    },
    {
      type: "input_value",
      name: "event",
      check: [Types.StepPreconditionsRaiseEvent],
    },
  ],
  inputsInline: true,
  output: Types.StepPreconditionsTimeout,
  colour: Colors.BlockPreconditions,
  tooltip: "Defines the timeout for waiting.",
};

export const step_preconditions_raise_event = {
  type: "step_preconditions_raise_event",
  message0: "raise event %1",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "event_name",
    },
  ],
  output: Types.StepPreconditionsRaiseEvent,
  colour: Colors.BlockPreconditions,
  tooltip: "The event to raise when timeout happened.",
};

export const step_preconditions_relational_expression = {
  type: "step_preconditions_relational_expression",
  message0: "expression %1 %2 %3",
  args0: [
    {
      type: "field_input",
      name: "input1",
      text: "value1",
    },
    {
      type: "field_dropdown",
      name: "values",
      options: [
        ["=", "="],
        ["!=", "!="],
        ["<", "<"],
        [">", ">"],
        ["<=", "<="],
        [">=", ">="],
      ],
    },
    {
      type: "field_input",
      name: "input2",
      text: "value2",
    },
  ],
  output: Types.StepPreconditionsExpression,
  colour: Colors.BlockPreconditions,
  tooltip: "A logic expression that evaluates to true or false.",
};