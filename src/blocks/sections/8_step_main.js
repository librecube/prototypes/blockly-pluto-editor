import * as Blockly from "blockly";
import { Colors } from "../../colors";
import { Types } from "../types";

export const all_step_main_types = [
  Types.StepMainSetContext,
  Types.StepMainAssignmentStatement,
  Types.StepMainLogic,
  Types.StepMainInitiateAndConfirmStep,
  Types.StepMainInitiateAndConfirmActivity,
  Types.StepMainInitiateActivity,
  Types.StepMainInformUser,
  Types.StepMainLog,
  Types.StepMainWaitUntilAbsoluteTime,
  Types.StepMainWaitForRelativeTime,
  Types.StepMainWaitForEvent,
  Types.StepMainWaitUntilTrue,
];

// step main body blocks
export const step_main_set_context = {
  type: "step_main_set_context",
  implicitAlign0: "RIGHT",
  message0: "in the context of %1 do %2 %3 end context %4",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "default",
    },
    {
      type: "input_dummy",
    },
    {
      type: "input_statement",
      name: "main",
      check: all_step_main_types,
    },
    {
      type: "input_dummy",
    },
  ],
  previousStatement: Types.StepMainSetContext,
  nextStatement: all_step_main_types,
  colour: Colors.BlockMain,
  tooltip: "Defines the context in which a set of step statements execute.",
};

export const step_main_assignment_statement = {
  type: "step_main_assignment_statement",
  message0: "assignment %1 := %2",
  args0: [
    {
      type: "field_input",
      name: "name",
      text: "variable_name",
    },
    {
      type: "field_input",
      name: "value",
      text: "value",
    },
  ],
  previousStatement: Types.StepMainAssignmentStatement,
  nextStatement: all_step_main_types,
  colour: Colors.BlockMain,
  tooltip: "Assign a value to a local variable.",
};

// flow control statements
export const step_main_logic_if = {
  type: "step_main_logic_if",
  message0: "if %1 then %2 %3",
  args0: [
    {
      type: "input_value",
      name: "expression",
      check: [Types.StepMainExpression],
    },
    {
      type: "input_end_row",
    },
    {
      type: "input_statement",
      name: "statement",
      check: all_step_main_types,
    },
  ],
  inputsInline: true,
  previousStatement: Types.StepMainLogic,
  nextStatement: all_step_main_types,
  colour: Colors.BlockMain,
  mutator: "step_main_logic_if_mutator",
  tooltip: "Execute one or more statements depending on the result of a logical condition.",
};

export const step_main_logic_if_optional_else_mutator = {
  type: "step_main_logic_if_optional_else_mutator",
  message0: "else %1",
  args0: [
    {
      type: "field_checkbox",
      name: "else",
      checked: false,
    },
  ],
  colour: Colors.BlockMain,
};

Blockly.Extensions.registerMutator(
  "step_main_logic_if_mutator",
  {
    saveExtraState: function () {
      return {
        attachedSections: this.attachedSections_,
      };
    },
    loadExtraState: function (state) {
      this.attachedSections_ = state["attachedSections"];
      this.updateShape_();
    },
    decompose: function (workspace) {
      var topBlock = workspace.newBlock(
        "step_main_logic_if_optional_else_mutator"
      );
      topBlock.initSvg();

      // Then we add one sub-block for each item in the list.
      this.attachedSections_ == true && topBlock.setFieldValue("TRUE", "else");

      return topBlock;
    },
    compose: function (topBlock) {
      var checkbox_declaration = topBlock.getFieldValue("else") === "TRUE";

      if (checkbox_declaration) {
        this.attachedSections_ = true;
      } else {
        this.attachedSections_ = false;
      }
      this.updateShape_();
    },
    updateShape_: function () {
      const appendField = () => {
        if (!this.getInput("else") && !this.getInput("else_statements")) {
          this.appendDummyInput("else").appendField("else");
          this.appendStatementInput("else_statements").setCheck(
            all_step_main_types
          );
        }
      };
      const deleteField = () => {
        if (this.getInput("else") && this.getInput("else_statements")) {
          this.removeInput("else");
          this.removeInput("else_statements");
        }
      };
      if (this.attachedSections_) {
        appendField();
      } else {
        deleteField();
      }
    },
  },
  () => {
    // console.log("mutator running....");
  },
  []
);

export const step_main_logic_expression = {
  type: "step_main_logic_expression",
  message0: "expression %1 %2 %3",
  args0: [
    {
      type: "field_input",
      name: "input1",
      text: "value1",
    },
    {
      type: "field_dropdown",
      name: "values",
      options: [
        ["=", "="],
        ["!=", "!="],
        ["<", "<"],
        [">", ">"],
        ["<=", "<="],
        [">=", ">="],
      ],
    },
    {
      type: "field_input",
      name: "input2",
      text: "value2",
    },
  ],
  output: Types.StepMainExpression,
  colour: Colors.BlockMain,
  tooltip: "A logic expression that evaluates to true or false.",
};

export const step_main_logic_repeat_statement = {
  type: "step_main_logic_repeat_statement",
  message0: "repeat %1 %2 until %3 %4",
  args0: [
    {
      type: "input_dummy",
    },
    {
      type: "input_statement",
      name: "statements",
      check: all_step_main_types,
    },
    {
      type: "input_value",
      name: "expression",
      check: [Types.StepMainExpression],
    },
    {
      type: "input_value",
      name: "timeout",
      check: [Types.StepMainWaitTimeout],
    },
  ],
  inputsInline: true,
  previousStatement: Types.StepMainLogic,
  nextStatement: all_step_main_types,
  colour: Colors.BlockMain,
  tooltip: "Repeat statements",
};

export const step_main_logic_case = {
  type: "step_main_logic_case",
  message0: "in case %1 %2 is %3 %4 %5",
  args0: [
    {
      type: "field_input",
      name: "expression",
      text: "expression",
    },
    {
      type: "input_dummy",
    },
    {
      type: "field_input",
      name: "case_tag",
      text: "case_tag",
    },
    {
      type: "input_dummy",
    },
    {
      type: "input_statement",
      name: "statements",
      check: all_step_main_types,
    },
  ],
  previousStatement: Types.StepMainLogic,
  nextStatement: all_step_main_types,
  colour: Colors.BlockMain,
  tooltip:
    "Multiple conditional branching depending on the value of an expression.",
  mutator: "step_main_logic_case_mutator",
};

export const step_main_logic_case_optional_parts_mutator = {
  type: "step_main_logic_case_optional_parts_mutator",
  message0: "or is %1 %2 otherwise %3",
  args0: [
    {
      type: "input_dummy",
    },
    {
      type: "input_statement",
      name: "STACK",
    },
    {
      type: "field_checkbox",
      name: "otherwise",
      checked: false,
    },
  ],
  colour: Colors.BlockMain,
};

export const step_main_logic_case_or_is_block = {
  type: "step_main_logic_case_or_is_block",
  message0: "or is",
  previousStatement: null,
  nextStatement: null,
  colour: Colors.BlockMain,
  tooltip: "or is block",
};

Blockly.Extensions.registerMutator(
  "step_main_logic_case_mutator",

  {
    saveExtraState: function () {
      return {
        itemCount: this.itemCount_,
        casetags: this.casetags_,
        otherwise: this.otherwise_,
      };
    },

    loadExtraState: function (state) {
      this.itemCount_ = state["itemCount"];
      this.otherwise_ = state["otherwise"];
      this.casetags_ = state["casetags"];
      this.updateShape_();
    },

    decompose: function (workspace) {
      var topBlock = workspace.newBlock(
        "step_main_logic_case_optional_parts_mutator"
      );
      topBlock.initSvg();

      // for or is
      var connection = topBlock.getInput("STACK").connection;
      for (var i = 0; i < this.itemCount_; i++) {
        var child = workspace.newBlock("step_main_logic_case_or_is_block");
        child.initSvg();
        connection.connect(child.previousConnection);
        connection = child.nextConnection;
      }
      this.otherwise_ == true && topBlock.setFieldValue("TRUE", "otherwise");
      return topBlock;
    },

    compose: function (topBlock) {
      var itemBlock = topBlock.getInputTargetBlock("STACK");
      var Connections = [],
        Values = [];
      while (itemBlock && !itemBlock.isInsertionMarker()) {
        Connections.push(itemBlock.valueConnection_);
        Values.push(itemBlock.valueValue_ || "");
        itemBlock =
          itemBlock.nextConnection && itemBlock.nextConnection.targetBlock();
      }

      this.itemCount_ = Connections.length;
      this.casetags_ = Values;
      this.otherwise_ = topBlock.getFieldValue("otherwise") == "TRUE";
      this.updateShape_();
      for (var i = 0; i < this.itemCount_; i++) {
        if (Values[i]) this.setFieldValue(Values[i], "OR_IS_VALUE" + i);
        if (
          Connections[i] &&
          this.getInput("OR_IS" + i) &&
          this.getInput("OR_IS" + i).connection
        ) {
          Connections[i]?.reconnect(this, "OR_IS" + i);
        }
      }
    },

    saveConnections: function (topBlock) {
      function save_connection(block, name) {
        var itemBlock = topBlock.getInputTargetBlock("STACK");
        var i = 0;
        while (itemBlock) {
          var input = block.getInput(name + i);
          itemBlock.valueConnection_ =
            input && input.connection && input.connection.targetConnection;
          itemBlock.valueValue_ = block.getFieldValue(`${name}_VALUE` + i);
          i++;
          itemBlock =
            itemBlock.nextConnection && itemBlock.nextConnection.targetBlock();
        }
      }
      save_connection(this, "OR_IS");
    },
    updateShape_: function () {
      // Remove existing inputs
      for (var i = 0; this.getInput("OR_IS" + i); i++) {
        this.removeInput("OR_IS" + i);
      }
      for (var i = 0; this.getInput("OR_IS_TEXT" + i); i++) {
        this.removeInput("OR_IS_TEXT" + i);
      }
      // Add new inputs
      for (var i = 0; i < this.itemCount_; i++) {
        this.appendDummyInput("OR_IS_TEXT" + i)
          .appendField("or is")
          .appendField(
            new Blockly.FieldTextInput(
              this.casetags_[i] != "" ? this.casetags_[i] : "case_tag"
            ),
            "OR_IS_VALUE" + i
          );
        var input = this.appendStatementInput("OR_IS" + i).setCheck(
          all_step_main_types
        );
      }
      const appendField = () => {
        if (
          !this.getInput("otherwise") &&
          !this.getInput("otherwise_statements")
        ) {
          this.appendDummyInput("otherwise").appendField("otherwise");
          this.appendStatementInput("otherwise_statements").setCheck(
            all_step_main_types
          );
        }
      };
      const deleteField = () => {
        if (
          this.getInput("otherwise") &&
          this.getInput("otherwise_statements")
        ) {
          this.removeInput("otherwise");
          this.removeInput("otherwise_statements");
        }
      };
      if (this.otherwise_) {
        appendField();
        this.moveInputBefore("otherwise", null);
        this.moveInputBefore("otherwise_statements", null);
      } else {
        deleteField();
      }
    },
  },

  () => {
    // console.log("mutator running....");
  },

  ["step_main_logic_case_or_is_block"]
);

export const step_main_logic_while = {
  type: "step_main_logic_while",
  message0: "while %1 %2 do %3",
  args0: [
    {
      type: "input_value",
      name: "expression",
      check: [Types.StepMainExpression],
    },
    {
      type: "input_value",
      name: "timeout",
      check: [Types.StepMainWaitTimeout],
    },
    {
      type: "input_statement",
      name: "statements",
      check: all_step_main_types,
    },
  ],
  previousStatement: Types.StepMainLogic,
  nextStatement: all_step_main_types,
  colour: Colors.BlockMain,
  inputsInline: true,
  tooltip:
    "Conditional iteration of a statement (or set of statements) with control at the beginning, optionally terminated by a timeout.",
};

export const step_main_logic_for_statement = {
  type: "step_main_logic_for_statement",
  message0: "for %1 := %2 to %3 by %4 do %5 %6",
  args0: [
    {
      type: "field_input",
      name: "variable_name",
      text: "variable_name",
    },
    {
      type: "field_input",
      name: "expression",
      text: "value",
    },
    {
      type: "field_input",
      name: "expression2",
      text: "value",
    },
    {
      type: "field_input",
      name: "increment",
      text: "1",
    },
    {
      type: "input_dummy",
      name: "dummy",
    },
    {
      type: "input_statement",
      name: "statements",
      check: all_step_main_types,
    },
  ],
  previousStatement: Types.StepMainLogic,
  nextStatement: all_step_main_types,
  colour: Colors.BlockMain,
  tooltip:
    "Iterates the execution of a statement (or set of statements) a fixed number of times.",
};

// wait statements
export const step_main_wait_until_absolute_time = {
  type: "step_main_wait_until_absolute_time",
  message0: "wait until absolute time %1",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "yyyy-mm-ddThh:mm:ss.sssZ",
    },
  ],
  previousStatement: Types.StepMainWaitUntilAbsoluteTime,
  nextStatement: all_step_main_types,
  colour: Colors.BlockMain,
  tooltip: "Wait until a given absolute time",
};

export const step_main_wait_for_relative_time = {
  type: "step_main_wait_for_relative_time",
  message0: "wait for relative time %1",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "relative_time",
    },
  ],
  previousStatement: Types.StepMainWaitForRelativeTime,
  nextStatement: all_step_main_types,
  colour: Colors.BlockMain,
  tooltip: "Wait for a given time to elapse",
};

export const step_main_wait_for_event = {
  type: "step_main_wait_for_event",
  message0: "wait for event %1",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "event_name",
    },
  ],
  previousStatement: Types.StepMainWaitForEvent,
  nextStatement: all_step_main_types,
  colour: Colors.BlockMain,
  tooltip: "Wait for a given event to occur",
};

export const step_main_wait_until_true = {
  type: "step_main_wait_until_true",
  message0: "wait until true %1 %2",
  args0: [
    {
      type: "input_value",
      name: "expression",
      check: [Types.StepMainExpression],
    },
    {
      type: "input_value",
      name: "extra",
      check: [Types.StepMainWaitTimeout],
    },
  ],
  inputsInline: true,
  previousStatement: Types.StepMainWaitUntilTrue,
  nextStatement: all_step_main_types,
  colour: Colors.BlockMain,
  tooltip: "Wait until a given conditions become true",
};

export const step_main_wait_timeout = {
  type: "step_main_wait_timeout",
  message0: "timeout %1 %2",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "relative_time",
    },
    {
      type: "input_value",
      name: "event",
      check: [Types.StepMainRaiseEvent],
    },
  ],
  inputsInline: true,
  output: Types.StepMainWaitTimeout,
  colour: Colors.BlockMain,
  tooltip: "Defines the timeout for waiting",
};

export const step_main_raise_event = {
  type: "step_main_raise_event",
  message0: "raise event %1",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "event_name",
    },
  ],
  output: Types.StepMainRaiseEvent,
  colour: Colors.BlockMain,
  tooltip: "The event to raise when timeout happened",
};



export const step_main_initiate_and_confirm_step = {
  type: "step_main_initiate_and_confirm_step",
  message0: "initiate and confirm step  %1 \n %2 \n %3",
  args0: [
    {
      type: "field_input",
      name: "step_name",
      text: "step_name",
    },
    {
      type: "input_value",
      name: "step_definition",
      check: [
        Types.ProcedureMainInitiateAndConfirmStepDefinitionPlaceholder,
        Types.ProcedureMainInitiateAndConfirmStepDefinition,
      ],
    },
    {
      type: "input_value",
      name: "continuation_test",
      check: Types.ProcedureMainInitiateAndConfirmStepContinuationTest,
    },
  ],
  previousStatement: Types.StepMainInitiateAndConfirmStep,
  nextStatement: all_step_main_types,
  inputsInline: true,
  colour: Colors.BlockMain,
  tooltip: "initiate and confirm step block",
};

export const step_main_initiate_and_confirm_activity = {
  type: "step_main_initiate_and_confirm_activity",
  message0: "initiate and confirm %1 \n %2 \n %3",
  args0: [
    {
      type: "field_input",
      name: "activity_call",
      text: "activity_call",
    },
    {
      type: "input_value",
      name: "refer_by",
      check: [Types.ProcedureMainInitiateActivityReferBy],
    },
    {
      type: "input_value",
      name: "continuation_test",
      check: Types.ProcedureMainInitiateAndConfirmStepContinuationTest,
    },
  ],
  inputsInline: true,
  previousStatement: Types.StepMainInitiateAndConfirmActivity,
  nextStatement: all_step_main_types,
  colour: Colors.BlockMain,
  tooltip: "A initiate and confirm Activity statement",
  mutator: "procedure_main_initiate_and_confirm_activity_mutator",
};

export const step_main_initiate_activity = {
  type: "step_main_initiate_activity",
  message0: "initiate %1 %2",
  args0: [
    {
      type: "field_input",
      name: "activity_call",
      text: "activity_call",
    },
    {
      type: "input_value",
      name: "refer_by",
      check: [Types.ProcedureMainInitiateActivityReferBy],
    },
  ],
  inputsInline: true,
  previousStatement: Types.StepMainInitiateActivity,
  nextStatement: all_step_main_types,
  colour: Colors.BlockMain,
  tooltip: "A initiate Activity statement",
  mutator: "procedure_main_initiate_activity_mutator",
};

export const step_main_inform_user = {
  type: "step_main_inform_user",
  message0: "inform user %1",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "text",
    },
  ],
  previousStatement: Types.StepMainInformUser,
  nextStatement: all_step_main_types,
  colour: Colors.BlockMain,
  tooltip: "Inform user without blocking procedure execution",
};

export const step_main_log = {
  type: "step_main_log",
  message0: "log %1",
  args0: [
    {
      type: "field_input",
      name: "value",
      text: "text",
    },
  ],
  previousStatement: Types.StepMainLog,
  nextStatement: all_step_main_types,
  colour: Colors.BlockMain,
  tooltip: "A log statement",
};