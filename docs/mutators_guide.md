# How mutators work

---

**blockly mutators are used to change the block structure based on the blocks combinations present in mutator workspace.
for example if we want more than one the inputs then we can use mutator to change the block structure.**

---

Lets understand mutators with the following example:

1. Step 1: define mutator in JSON definition of the block

```javascript

export const custom_block = {
 ...
  mutator: "custom mutator",
};

```

In above definition we defined the `custom_mutator` mutator, lets implement that mutator

2.  Step 2: defining the mutator

before going further lets understand the structure of mutator

```javascript
// Function signature.
Blockly.Extensions.registerMutator(name, mixinObj, opt_helperFn, opt_blockList);

// Example call.
Blockly.Extensions.registerMutator(
  "controls_if_mutator",
  {
    /* mutator methods */
  },
  undefined, // callback method
  ["controls_if_elseif", "controls_if_else"]
);
```

Mutators are parts of extensions `registerMutator()` function takes 4 parameters as following:

1. Name of the mutator
2. Various methods, we will see later
3. Callback function, which gets executed every time mutator runs
4. Array of blocks shown in the toolbox of the mutator

here's the mutator for continuation test block

```javascript
Blockly.Extensions.registerMutator(
"procedure_main_initiate_and_confirm_step_continuation_test_mutator",
  {

    saveExtraState: function () {
      return { ...};
    },
   
    loadExtraState: function (state) {..},
   
    decompose: function (workspace) {
      var topBlock =   workspace.newBlock("procedure_main_initiate_and_confirm_step_continuation_test_parts_mutator");
 
      topBlock.initSvg();
      return topBlock;

    },
    compose: function (topBlock) {...},
   
    saveConnections: function (topBlock) { ... },

    updateShape_: function () {...},

  },

  () => {
    console.log("mutator running....")
  },

  [

    "procedure_main_initiate_and_confirm_activity_confirmed",

    "procedure_main_initiate_and_confirm_activity_not_confirmed",

    "procedure_main_initiate_and_confirm_activity_aborted",

  ]

);
```

Lets understand all method which are present in the above mutator:

- saveExtraState: As we modify the structure of the block through a mutator, the block should maintain the extra states, which can be used to retrieve the modified block's structure after reloading the page.
- loadExtraState: Before running the mutator on block this method is used to load the extra saved states into block for creating correct modified block.
- decompose: this method is used to initiate the first block in the mutator workspace, this function returns the `topBlock` which gets sent to decompose method.
- compose: this method receives `topBlock` as parameter then using extra states it connects other blocks to the parent block that is present in the mutator workspace.
- saveConnections(optional): this method is called before the decompose, this is used to map the connections of main block to the blocks which are present in mutator workspace
- updateShape\_(): this method is used to update the shape of the actual block using extra states.

### Complete example of mutator

- block definition:

```javascript
export const procedure_main_initiate_and_confirm_step_continuation_test = {
  type: "procedure_main_initiate_and_confirm_step_continuation_test",
  message0: "continuation test",
  output: Types.ProcedureMainInitiateAndConfirmStepContinuationTest,
  colour: Colors.BlockMain,
  tooltip: "continuation test block",
  mutator: "procedure_main_initiate_and_confirm_step_continuation_test_mutator",
};
```

- mutator definition:

```javascript
Blockly.Extensions.registerMutator(
  "procedure_main_initiate_and_confirm_step_continuation_test_mutator",
  {
    saveExtraState: function () {
      return {
        itemCountContinuationTest: this.itemCountContinuationTest_,
        ContinuationTest: this.ContinuationTest_,
      };
    },

    loadExtraState: function (state) {
      this.itemCountContinuationTest_ = state["itemCountContinuationTest"];
      this.ContinuationTest_ = state["ContinuationTest"];
      this.updateShape_();
    },

    decompose: function (workspace) {
      // creating new block for mutator workspace
      var topBlock = workspace.newBlock(
        "procedure_main_initiate_and_confirm_step_continuation_test_parts_mutator"
      );
      topBlock.initSvg();

      // initializing child blocks using saved states
      var connection = topBlock.getInput("CONTINUATION_TEST_STACK").connection;
      for (var i = 0; i < this.itemCountContinuationTest_; i++) {
        var child = workspace.newBlock(
          "procedure_main_initiate_and_confirm_activity_confirmed"
        );
        if (this.ContinuationTest_[i].name == "not confirmed") {
          child = workspace.newBlock(
            "procedure_main_initiate_and_confirm_activity_not_confirmed"
          );
        } else if (this.ContinuationTest_[i].name == "aborted") {
          child = workspace.newBlock(
            "procedure_main_initiate_and_confirm_activity_aborted"
          );
        }

        child.initSvg();
        connection.connect(child.previousConnection);
        connection = child.nextConnection;
      }

      return topBlock;
    },

    compose: function (topBlock) {
      // getting input of top block
      var itemBlockContinuationTest = topBlock.getInputTargetBlock(
        "CONTINUATION_TEST_STACK"
      );
      // empty array for storing values of continuation test
      var ContinuationTestConnections = [],
        ContinuationTestNames = [],
        ContinuationTestValues = [];
      // filling arrays with appropriate values
      while (
        itemBlockContinuationTest &&
        !itemBlockContinuationTest.isInsertionMarker()
      ) {
        ContinuationTestConnections.push(
          itemBlockContinuationTest.valueConnection_
        );
        ContinuationTestNames.push(
          itemBlockContinuationTest.type ==
            "procedure_main_initiate_and_confirm_activity_confirmed"
            ? "confirmed"
            : itemBlockContinuationTest.type ==
              "procedure_main_initiate_and_confirm_activity_not_confirmed"
            ? "not confirmed"
            : "aborted"
        );
        ContinuationTestValues.push(
          itemBlockContinuationTest.valueValue_ || ""
        );
        itemBlockContinuationTest =
          itemBlockContinuationTest.nextConnection &&
          itemBlockContinuationTest.nextConnection.targetBlock();
      }

      this.itemCountContinuationTest_ = ContinuationTestConnections.length;
      this.ContinuationTest_ = ContinuationTestNames.map((name, index) => ({
        name: name,
        value: ContinuationTestValues[index],
      }));
      // update shape of the block
      this.updateShape_();
      // setting field values of inputs of main block
      for (var i = 0; i < this.itemCountContinuationTest_; i++) {
        if (ContinuationTestNames[i])
          this.setFieldValue(ContinuationTestNames[i], "CONTINUATION_NAME" + i);
        if (ContinuationTestValues[i])
          this.setFieldValue(
            ContinuationTestValues[i],
            "CONTINUATION_VALUE" + i
          );
        // reconnecting connections of main block to the blocks which are
        // present in mutator workspace
        if (
          ContinuationTestValues[i] &&
          this.getInput("CONTINUATION" + i) &&
          this.getInput("CONTINUATION" + i).connection
        ) {
          ContinuationTestConnections[i]?.reconnect(this, "CONTINUATION" + i);
        }
      }
    },

    saveConnections: function (topBlock) {
      // getting input of top block
      var itemBlockContinuationTest = topBlock.getInputTargetBlock(
        "CONTINUATION_TEST_STACK"
      );
      var j = 0;
      // saving connections from main block to the blocks which are
      // present in mutator workspace
      while (itemBlockContinuationTest) {
        var input = this.getInput("CONTINUATION" + j);
        itemBlockContinuationTest.valueConnection_ =
          input && input.connection && input.connection.targetConnection;
        itemBlockContinuationTest.valueValue_ = this.getFieldValue(
          `CONTINUATION_VALUE` + j
        );
        j++;
        itemBlockContinuationTest =
          itemBlockContinuationTest.nextConnection &&
          itemBlockContinuationTest.nextConnection.targetBlock();
      }
    },
    updateShape_: function () {
      // Remove existing continuation inputs
      for (var i = 0; this.getInput("END_ROW_CONTINUATION_TEST" + i); i++) {
        this.removeInput("END_ROW_CONTINUATION_TEST" + i);
      }
      for (var i = 0; this.getInput("CONTINUATION" + i); i++) {
        this.removeInput("CONTINUATION" + i);
      }

      // Add new inputs for continuations
      for (var i = 0; i < this.itemCountContinuationTest_; i++) {
        var input = this.appendDummyInput("CONTINUATION" + i)
          .appendField(this.ContinuationTest_[i].name, "CONTINUATION_NAME" + i)
          .appendField(":")
          .appendField(
            new Blockly.FieldTextInput(
              this.ContinuationTest_[i] == ""
                ? this.ContinuationTest_[i].value
                : "value"
            ),
            "CONTINUATION_VALUE" + i
          );
        this.appendEndRowInput("END_ROW_CONTINUATION_TEST" + i);
      }

      // moving inputs at the end of the block
      if (this.getInput("IN_CASE") && this.getInput("CONTINUATION0")) {
        this.moveInputBefore("CONTINUATION_END_ROW", "CONTINUATION0");
        this.moveInputBefore("IN_CASE", "CONTINUATION0");
        this.moveInputBefore("CONTINUATION_END_ROW_AFTER", "CONTINUATION0");
      }

      // remove unnecessary inputs and update shape
      if (this.itemCountContinuationTest_ == 0) {
        this.getInput("IN_CASE") && this.removeInput("IN_CASE");
        this.getInput("END_ROW") && this.removeInput("END_ROW");
        this.getInput("CONTINUATION_END_ROW") &&
          this.removeInput("CONTINUATION_END_ROW");
        this.getInput("CONTINUATION_END_ROW_AFTER") &&
          this.removeInput("CONTINUATION_END_ROW_AFTER");
      }
    },
  },
  // callback function
  () => {
    // console.log("mutator running....",this)
  },
  // array of blocks which will be shown in toolbox of mutator
  [
    "procedure_main_initiate_and_confirm_activity_confirmed",
    "procedure_main_initiate_and_confirm_activity_not_confirmed",
    "procedure_main_initiate_and_confirm_activity_aborted",
  ]
);
```

---

reference:
https://developers.google.com/blockly/guides/create-custom-blocks/extensions
