import * as Blockly from "blockly";
import { Colors } from "../../colors";
import { Types } from "../types";
import { all_step_main_types } from "./8_step_main";
import { all_step_preconditions_types } from "./7_step_preconditions";
import { all_step_declaration_types } from "./6_step_declaration";
import { all_step_confirmation_types } from "./10_step_confirmation";

export const all_procedure_watchdog_types = [
  Types.ProcedureWatchdogInitiateAndConfirmStep,
];

export const procedure_watchdog_initiate_and_confirm_step = {
  type: "procedure_watchdog_initiate_and_confirm_step",
  message0: "initiate and confirm step  %1 \n %2 \n %3",
  args0: [
    {
      type: "field_input",
      name: "step_name",
      text: "step_name",
    },
    {
      type: "input_value",
      name: "step_definition",
      check: [
        Types.ProcedureWatchdogInitiateAndConfirmStepDefinition,
      ],
    },
    {
      type: "input_value",
      name: "continuation_test",
      check: Types.ProcedureMainInitiateAndConfirmStepContinuationTest,
    },
  ],
  previousStatement: Types.ProcedureWatchdogInitiateAndConfirmStep,
  nextStatement: [
    Types.ProcedureWatchdogInitiateAndConfirmStep,
    Types.ProcedureWatchdog,
  ],
  inputsInline: true,
  colour: Colors.BlockWatchdog,
  tooltip: "Initiate and confirm execution of the steps that compose the watchdog body (called watchdog steps). All steps are executed in parallel.",
};

export const procedure_watchdog_initiate_and_confirm_step_definition = {
  type: "procedure_watchdog_initiate_and_confirm_step_definition",
  message0: "Watchdog Step \n preconditions \n %1 main %2 %3",
  args0: [
    {
      type: "input_statement",
      name: "preconditions",
      check: all_step_preconditions_types,
    },
    {
      type: "input_dummy",
      name: "main_name",
    },
    {
      type: "input_statement",
      name: "main",
      check: all_step_main_types,
    },
  ],
  output: Types.ProcedureWatchdogInitiateAndConfirmStepDefinition,
  colour: Colors.BlockWatchdog,
  mutator: "procedure_watchdog_initiate_and_confirm_step_mutator",
};

export const procedure_watchdog_optional_steps_sections_mutator = {
  type: "procedure_watchdog_optional_steps_sections_mutator",
  message0: "declaration %1 %2 confirmation %3",
  args0: [
    {
      type: "field_checkbox",
      name: "declaration",
      checked: false,
    },
    {
      type: "input_dummy",
    },
    // {
    //   type: "field_checkbox",
    //   name: "watchdog",
    //   checked: false,
    // },
    // {
    //   type: "input_dummy",
    // },
    {
      type: "field_checkbox",
      name: "confirmation",
      checked: false,
    },
  ],
  colour: Colors.BlockWatchdog,
};

Blockly.Extensions.registerMutator(
  "procedure_watchdog_initiate_and_confirm_step_mutator",
  {
    saveExtraState: function () {
      return {
        attachedSections: this.attachedSections_,
      };
    },
    loadExtraState: function (state) {
      this.attachedSections_ = state["attachedSections"];
      this.updateShape_();
    },
    decompose: function (workspace) {
      var topBlock = workspace.newBlock(
        "procedure_watchdog_optional_steps_sections_mutator"
      );
      topBlock.initSvg();

      // Then we add one sub-block for each item in the list.
      this.attachedSections_?.includes("declaration") &&
        topBlock.setFieldValue("TRUE", "declaration");
      this.attachedSections_?.includes("watchdog") &&
        topBlock.setFieldValue("TRUE", "watchdog");
      this.attachedSections_?.includes("confirmation") &&
        topBlock.setFieldValue("TRUE", "confirmation");

      return topBlock;
    },
    compose: function (topBlock) {
      var checkbox_declaration =
        topBlock.getFieldValue("declaration") === "TRUE";
      var checkbox_watchdog = topBlock.getFieldValue("watchdog") === "TRUE";
      var checkbox_confirmation =
        topBlock.getFieldValue("confirmation") === "TRUE";

      // Then we collect up all of the connections of on our main block that are
      // referenced by our sub-blocks.
      // This relates to the saveConnections hook (explained below).
      var connections = [];
      if (checkbox_declaration) {
        connections.push("declaration");
      }
      if (checkbox_watchdog) {
        connections.push("watchdog");
      }
      if (checkbox_confirmation) {
        connections.push("confirmation");
      }

      // Then we disconnect any children where the sub-block associated with that
      // child has been deleted/removed from the stack. (pending)

      // Then we update the shape of our block (removing or adding inputs as necessary).
      // `this` refers to the main block.
      this.attachedSections_ = connections;
      this.updateShape_();
    },
    updateShape_: function () {
      if (!this.attachedSections_) {
        return;
      }
      const types = {
        declaration: all_step_declaration_types,
        activity_reference: all_step_preconditions_types,
        main: all_step_main_types,
        watchdog: [null],
        confirmation: all_step_confirmation_types,
      };
      const sections = [
        "declaration",
        "activity_reference",
        "main",
        // "watchdog",
        "confirmation",
      ];
      const appendField = (name, statement_input, optional = true) => {
        if (!this.getInput(name) && !this.getInput(statement_input)) {
          this.appendDummyInput(name).appendField(
            `${statement_input} ${optional ? "" : ""}`
          );
          this.appendStatementInput(statement_input).setCheck(
            types[statement_input]
          );
        }
      };
      const deleteField = (name, statement_input) => {
        if (this.getInput(name) && this.getInput(statement_input)) {
          this.removeInput(name);
          this.removeInput(statement_input);
        }
      };
      // console.log("updating shape");

      sections.forEach((name, i, arr) => {
        var connections = [];
        var connection_values = [];
        if (this.attachedSections_?.includes(name)) {
          for (var k = i + 1; k < sections.length; k++) {
            if (this.getInput(sections[k])) {
              connections.push(
                this.getInput(sections[k]).connection?.targetConnection
              );
              connection_values.push(sections[k]);
              deleteField(`${sections[k]}_name`, sections[k]);
            }
          }
          appendField(`${name}_name`, name);
          // console.log(connection_values);
          connection_values.forEach((value, j, arr) => {
            if (value == "main") appendField(`${value}_name`, value, false);
            else appendField(`${value}_name`, value);
            connections[j]?.reconnect(this, value);
          });
        } else {
          if (name != "main" && name != "activity_reference")
            deleteField(`${name}_name`, name);
        }
      });
    },
  },
  () => {
    // console.log("mutator running....");
  },
  []
);

export const procedure_watchdog_initiate_and_confirm_step_continuation_test = {
  type: "procedure_watchdog_initiate_and_confirm_step_continuation_test",
  message0: "continuation test",
  output: Types.ProcedureMainInitiateAndConfirmStepContinuationTest,
  colour: Colors.BlockWatchdog,
  tooltip: "continuation test block",
  mutator:
    "procedure_watchdog_initiate_and_confirm_step_continuation_test_mutator",
};

Blockly.Extensions.registerMutator(
  "procedure_watchdog_initiate_and_confirm_step_continuation_test_mutator",
  {
    saveExtraState: function () {
      return {
        itemCountContinuationTest: this.itemCountContinuationTest_,
        ContinuationTest: this.ContinuationTest_,
      };
    },

    loadExtraState: function (state) {
      this.itemCountContinuationTest_ = state["itemCountContinuationTest"];
      this.ContinuationTest_ = state["ContinuationTest"];
      this.updateShape_();
    },

    decompose: function (workspace) {
      var topBlock = workspace.newBlock(
        "procedure_main_initiate_and_confirm_step_continuation_test_parts_mutator"
      );
      topBlock.initSvg();

      // for continuation test
      var connection = topBlock.getInput("CONTINUATION_TEST_STACK").connection;
      for (var i = 0; i < this.itemCountContinuationTest_; i++) {
        var child = workspace.newBlock(
          "procedure_main_initiate_and_confirm_activity_confirmed"
        );
        if (this.ContinuationTest_[i].name == "not confirmed") {
          child = workspace.newBlock(
            "procedure_main_initiate_and_confirm_activity_not_confirmed"
          );
        } else if (this.ContinuationTest_[i].name == "aborted") {
          child = workspace.newBlock(
            "procedure_main_initiate_and_confirm_activity_aborted"
          );
        }
        child.initSvg();
        connection.connect(child.previousConnection);
        connection = child.nextConnection;
      }

      return topBlock;
    },

    compose: function (topBlock) {
      var itemBlockContinuationTest = topBlock.getInputTargetBlock(
        "CONTINUATION_TEST_STACK"
      );

      var ContinuationTestConnections = [],
        ContinuationTestNames = [],
        ContinuationTestValues = [];

      while (
        itemBlockContinuationTest &&
        !itemBlockContinuationTest.isInsertionMarker()
      ) {
        ContinuationTestConnections.push(
          itemBlockContinuationTest.valueConnection_
        );
        ContinuationTestNames.push(
          itemBlockContinuationTest.type ==
            "procedure_main_initiate_and_confirm_activity_confirmed"
            ? "confirmed"
            : itemBlockContinuationTest.type ==
              "procedure_main_initiate_and_confirm_activity_not_confirmed"
              ? "not confirmed"
              : "aborted"
        );
        ContinuationTestValues.push(
          itemBlockContinuationTest.valueValue_ || ""
        );
        itemBlockContinuationTest =
          itemBlockContinuationTest.nextConnection &&
          itemBlockContinuationTest.nextConnection.targetBlock();
      }

      this.itemCountContinuationTest_ = ContinuationTestConnections.length;
      this.ContinuationTest_ = ContinuationTestNames.map((name, index) => ({
        name: name,
        value: ContinuationTestValues[index],
      }));

      this.updateShape_();

      for (var i = 0; i < this.itemCountContinuationTest_; i++) {
        if (ContinuationTestNames[i])
          this.setFieldValue(ContinuationTestNames[i], "CONTINUATION_NAME" + i);
        if (ContinuationTestValues[i])
          this.setFieldValue(
            ContinuationTestValues[i],
            "CONTINUATION_VALUE" + i
          );
        if (
          ContinuationTestValues[i] &&
          this.getInput("CONTINUATION" + i) &&
          this.getInput("CONTINUATION" + i).connection
        ) {
          ContinuationTestConnections[i]?.reconnect(this, "CONTINUATION" + i);
        }
      }
    },

    saveConnections: function (topBlock) {
      var itemBlockContinuationTest = topBlock.getInputTargetBlock(
        "CONTINUATION_TEST_STACK"
      );
      var j = 0;
      while (itemBlockContinuationTest) {
        var input = this.getInput("CONTINUATION" + j);
        itemBlockContinuationTest.valueConnection_ =
          input && input.connection && input.connection.targetConnection;
        itemBlockContinuationTest.valueValue_ = this.getFieldValue(
          `CONTINUATION_VALUE` + j
        );
        j++;
        itemBlockContinuationTest =
          itemBlockContinuationTest.nextConnection &&
          itemBlockContinuationTest.nextConnection.targetBlock();
      }
    },
    updateShape_: function () {
      for (var i = 0; this.getInput("END_ROW_CONTINUATION_TEST" + i); i++) {
        this.removeInput("END_ROW_CONTINUATION_TEST" + i);
      }
      for (var i = 0; this.getInput("CONTINUATION" + i); i++) {
        this.removeInput("CONTINUATION" + i);
      }

      // // update shape for continuations
      // if (!this.getInput("IN_CASE") && this.itemCountContinuationTest_ > 0) {
      //   this.appendEndRowInput("CONTINUATION_END_ROW");
      //   this.appendDummyInput("IN_CASE").appendField("case");
      //   this.appendEndRowInput("CONTINUATION_END_ROW_AFTER");
      // }

      // Add new inputs for continuations
      for (var i = 0; i < this.itemCountContinuationTest_; i++) {
        var input = this.appendDummyInput("CONTINUATION" + i)
          .appendField(this.ContinuationTest_[i].name, "CONTINUATION_NAME" + i)
          .appendField(":")
          .appendField(
            new Blockly.FieldDropdown([
              ["resume", "resume"],
              ["abort", "abort"],
              ["restart", "restart"],
              ["ask_user", "ask user"],
              ["continue", "continue"],
              ["terminate", "terminate"],
            ]),
            "CONTINUATION_VALUE" + i
          );
        this.appendEndRowInput("END_ROW_CONTINUATION_TEST" + i);
      }

      if (this.getInput("IN_CASE") && this.getInput("CONTINUATION0")) {
        this.moveInputBefore("CONTINUATION_END_ROW", "CONTINUATION0");
        this.moveInputBefore("IN_CASE", "CONTINUATION0");
        this.moveInputBefore("CONTINUATION_END_ROW_AFTER", "CONTINUATION0");
      }

      if (this.itemCountContinuationTest_ == 0) {
        this.getInput("IN_CASE") && this.removeInput("IN_CASE");
        this.getInput("END_ROW") && this.removeInput("END_ROW");
        this.getInput("CONTINUATION_END_ROW") &&
          this.removeInput("CONTINUATION_END_ROW");
        this.getInput("CONTINUATION_END_ROW_AFTER") &&
          this.removeInput("CONTINUATION_END_ROW_AFTER");
      }
    },
  },
  () => {
    // console.log("mutator running....",this)
  },
  [
    "procedure_main_initiate_and_confirm_activity_confirmed",
    "procedure_main_initiate_and_confirm_activity_not_confirmed",
    "procedure_main_initiate_and_confirm_activity_aborted",
  ]
);
